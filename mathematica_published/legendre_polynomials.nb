(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 12.3' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       158,          7]
NotebookDataLength[     34802,        917]
NotebookOptionsPosition[     28719,        833]
NotebookOutlinePosition[     29207,        851]
CellTagsIndexPosition[     29164,        848]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{

Cell[CellGroupData[{
Cell["Mathematica und die Legendresche DGL", "Subsection",
 CellChangeTimes->{{3.829793570286504*^9, 3.8297935904487333`*^9}}],

Cell["Mathematica kennt nat\[UDoubleDot]rlich die Legendresche DGL:", "Text",
 CellChangeTimes->{{3.8297936048923483`*^9, 3.8297936312288723`*^9}, 
   3.829902257880277*^9}],

Cell[BoxData[
 RowBox[{"DSolve", "[", 
  RowBox[{
   RowBox[{
    RowBox[{
     RowBox[{
      RowBox[{"(", 
       RowBox[{"1", "-", 
        RowBox[{"x", "^", "2"}]}], ")"}], 
      RowBox[{
       RowBox[{"y", "''"}], "[", "x", "]"}]}], "-", 
     RowBox[{"2", "x", " ", 
      RowBox[{
       RowBox[{"y", "'"}], "[", "x", "]"}]}], "+", 
     RowBox[{"p", 
      RowBox[{"(", 
       RowBox[{"p", "+", "1"}], ")"}], 
      RowBox[{"y", "[", "x", "]"}]}]}], "\[Equal]", "0"}], ",", 
   RowBox[{"y", "[", "x", "]"}], ",", "x"}], "]"}]], "Input",
 CellChangeTimes->{{3.8297936348020487`*^9, 3.829793678042695*^9}},
 CellLabel->"In[13]:="],

Cell["\<\
mit LegendreP[] den Legendrepolynomen der 1. Art und LegendreQ[] den \
Legendrepolynomen der 2. Art. Anmerkung: Diese \[OpenCurlyQuote]offiziellen\
\[CloseCurlyQuote] L\[ODoubleDot]sungen sind wie im Fall der Airygleichung \
Linearkombinationen der Reihenloesungen, die man mit der Frobeniusmethode erh\
\[ADoubleDot]lt!

Solange man sich unter Legendrepolynomen nicht viel vorstellen kann, hilft \
das aber nicht sehr, daher verwenden wir Mathematica um bei einigen der m\
\[UDoubleDot]hsamen Rechnungen, die bei den \[OpenCurlyDoubleQuote]h\
\[ADoubleDot]ndischen\[CloseCurlyDoubleQuote] Betrachtungen auftreten, zu \
unterst\[UDoubleDot]tzen:\
\>", "Text",
 CellChangeTimes->{{3.829793799230796*^9, 3.829793926145525*^9}, {
  3.8299022727288275`*^9, 3.8299022860712733`*^9}, {3.829972474192892*^9, 
  3.8299725123455057`*^9}, {3.829972571664836*^9, 3.829972592747077*^9}, {
  3.829972631720109*^9, 3.8299726899340034`*^9}, {3.8470099330571203`*^9, 
  3.847009951916523*^9}},ExpressionUUID->"537b80e5-6ca4-4afc-a4f0-\
9a83bd129f86"],

Cell[CellGroupData[{

Cell["Hilfe mit der Rekursionsformel:", "Subsubsection",
 CellChangeTimes->{{3.8297939374557695`*^9, 3.8297939467548947`*^9}}],

Cell[TextData[{
 "In der VO wurde explizit vorgerechnet, dass ",
 Cell[BoxData[
  FormBox[
   SubscriptBox["a", 
    RowBox[{"k", "+", "2"}]], TraditionalForm]]],
 " = ",
 Cell[BoxData[
  FormBox[
   RowBox[{
    RowBox[{
     RowBox[{"[", 
      RowBox[{
       RowBox[{"k", "(", 
        RowBox[{"k", " ", "+", " ", "1"}], ")"}], " ", "-", " ", 
       RowBox[{"p", 
        RowBox[{"(", 
         RowBox[{"p", " ", "+", " ", "1"}], ")"}]}]}], "]"}], "/", 
     RowBox[{"[", 
      RowBox[{
       RowBox[{"(", 
        RowBox[{"k", " ", "+", " ", "1"}], ")"}], 
       RowBox[{"(", 
        RowBox[{"k", " ", "+", " ", "2"}], ")"}]}], "]"}]}], 
    SubscriptBox["a", "k"]}], TraditionalForm]]],
 "\nDer Befehl RecurrenceTable[] hilft dabei, die ersten Terme explizit zu \
erhalten."
}], "Text",
 CellChangeTimes->{{3.829794037763816*^9, 3.8297940571289563`*^9}, {
  3.829794090913272*^9, 3.82979427411957*^9}, {3.82990220963688*^9, 
  3.8299022246789417`*^9}, {3.8299023008651495`*^9, 3.829902323265678*^9}}],

Cell[BoxData[
 RowBox[{"ak", "=", 
  RowBox[{"RecurrenceTable", "[", 
   RowBox[{
    RowBox[{"{", 
     RowBox[{
      RowBox[{
       RowBox[{"a", "[", 
        RowBox[{"k", "+", "2"}], "]"}], "\[Equal]", " ", 
       RowBox[{
        RowBox[{
         RowBox[{"(", 
          RowBox[{
           RowBox[{"k", 
            RowBox[{"(", 
             RowBox[{"k", "+", "1"}], ")"}]}], "-", 
           RowBox[{"p", 
            RowBox[{"(", 
             RowBox[{"p", "+", "1"}], ")"}]}]}], ")"}], "/", 
         RowBox[{"(", 
          RowBox[{
           RowBox[{"(", 
            RowBox[{"k", "+", "1"}], ")"}], 
           RowBox[{"(", 
            RowBox[{"k", "+", "2"}], ")"}]}], ")"}]}], 
        RowBox[{"a", "[", "k", "]"}]}]}], ",", 
      RowBox[{
       RowBox[{"a", "[", "0", "]"}], "\[Equal]", "1"}], ",", 
      RowBox[{
       RowBox[{"a", "[", "1", "]"}], "\[Equal]", "1"}]}], "}"}], ",", "a", 
    ",", 
    RowBox[{"{", 
     RowBox[{"k", ",", "0", ",", "10"}], "}"}]}], "]"}]}]], "Input",
 CellChangeTimes->{{3.8297942367961974`*^9, 3.8297942457050915`*^9}, {
  3.829794282825127*^9, 3.829794363883718*^9}, {3.8297943939916167`*^9, 
  3.8297944312327633`*^9}, {3.829794623359889*^9, 3.829794629984964*^9}},
 CellLabel->"In[14]:="],

Cell[TextData[{
 "Auf diese Art lassen sich die Koeffizienten ",
 Cell[BoxData[
  FormBox[
   SubscriptBox["a", "k"], TraditionalForm]]],
 " startend von k=0 bzw. k=1 m\[UDoubleDot]helos generieren. Von den \
theoretischen \[CapitalUDoubleDot]berlegungen ist es klar, dass man zwei \
linear unabh\[ADoubleDot]ngige Reihenentwicklungen, eine mit \
ausschlie\[SZ]lich geraden Potenzen, die andere ausschlie\[SZ]lich mit \
ungeraden Potenzen erh\[ADoubleDot]lt. Bilden wir als n\[ADoubleDot]chstes \
die ersten Terme dieser Reihen. ",
 StyleBox["NB:",
  FontWeight->"Bold"],
 " die ",
 Cell[BoxData[
  FormBox[
   SubscriptBox["a", "k"], TraditionalForm]]],
 " in Variable ak gehen von k=0,10, die Indizierung von Mathematica beginnt \
aber mit 1, d.h.:)"
}], "Text",
 CellChangeTimes->{{3.8297945163727303`*^9, 3.8297946138575864`*^9}, {
  3.8297946486018724`*^9, 3.829794722774024*^9}, {3.829902360893853*^9, 
  3.8299024244846325`*^9}}],

Cell[BoxData[
 RowBox[{"ak", "[", 
  RowBox[{"[", "1", "]"}], "]"}]], "Input",
 CellChangeTimes->{{3.829794733780424*^9, 3.829794736187172*^9}},
 CellLabel->"In[15]:="],

Cell[BoxData[
 RowBox[{"ak", "[", 
  RowBox[{"[", "2", "]"}], "]"}]], "Input",
 CellChangeTimes->{{3.829794739210926*^9, 3.8297947417534714`*^9}},
 CellLabel->"In[16]:="],

Cell[BoxData[
 RowBox[{
  RowBox[{"ak", "[", 
   RowBox[{"[", "3", "]"}], "]"}], " ", 
  RowBox[{"(*", " ", 
   RowBox[{"Bitte", " ", "mit", " ", "oben", " ", 
    RowBox[{"vergleichen", "!"}]}], "*)"}]}]], "Input",
 CellChangeTimes->{{3.8297947430489187`*^9, 3.8297947667257376`*^9}},
 CellLabel->"In[17]:="],

Cell[BoxData[
 RowBox[{
  RowBox[{"-", 
   FractionBox["1", "2"]}], " ", "p", " ", 
  RowBox[{"(", 
   RowBox[{"1", "+", "p"}], ")"}]}]], "Input",
 CellChangeTimes->{{3.829794751770954*^9, 3.8297947537528105`*^9}},
 CellLabel->"In[18]:="],

Cell[BoxData[
 RowBox[{"soleven", "=", 
  RowBox[{"Sum", "[", 
   RowBox[{
    RowBox[{
     RowBox[{"ak", "[", 
      RowBox[{"[", 
       RowBox[{"k", "+", "1"}], "]"}], "]"}], 
     RowBox[{"x", "^", "k"}]}], ",", 
    RowBox[{"{", 
     RowBox[{"k", ",", "0", ",", "9", ",", "2"}], "}"}]}], "]"}], " ", 
  RowBox[{"(*", " ", 
   RowBox[{
    RowBox[{"gerade", " ", "Potentenzen"}], ",", " ", 
    RowBox[{
    "k", " ", "laeuft", " ", "von", " ", "0", " ", "bis", " ", "9", " ", "in",
      " ", "Inkrementen", " ", "von", " ", "2"}]}], " ", "*)"}]}]], "Input",
 CellChangeTimes->{{3.829794772317214*^9, 3.8297948681866884`*^9}},
 CellLabel->"In[19]:="],

Cell[BoxData[
 RowBox[{"solodd", "=", 
  RowBox[{"Sum", "[", 
   RowBox[{
    RowBox[{
     RowBox[{"ak", "[", 
      RowBox[{"[", 
       RowBox[{"k", "+", "1"}], "]"}], "]"}], 
     RowBox[{"x", "^", "k"}]}], ",", 
    RowBox[{"{", 
     RowBox[{"k", ",", "1", ",", "9", ",", "2"}], "}"}]}], "]"}], " ", 
  RowBox[{"(*", " ", 
   RowBox[{
    RowBox[{"ungerade", " ", "Potentenzen"}], ",", " ", 
    RowBox[{
    "k", " ", "laeuft", " ", "von", " ", "1", " ", "bis", " ", "9", " ", "in",
      " ", "Inkrementen", " ", "von", " ", "2"}]}], " ", "*)"}]}]], "Input",
 CellChangeTimes->{{3.829794772317214*^9, 3.829794897377036*^9}},
 CellLabel->"In[20]:="],

Cell[TextData[{
 "Soweit, so gut. F\[UDoubleDot]r die bis jetzt durchgef\[UDoubleDot]hrten \
Ableitungen kann p eine beliebige (positive) reelle Zahl sind, in den \
typischen Anwendungen ist p eine nat\[UDoubleDot]rliche Zahl. Man kann sich \
dann leicht aus der Rekursion \[UDoubleDot]berlegen, dass f\[UDoubleDot]r k=p \
der Koeffizient ",
 Cell[BoxData[
  FormBox[
   SubscriptBox["a", 
    RowBox[{"k", "+", "2"}]], TraditionalForm]],ExpressionUUID->
  "dd40efc5-9510-4c5a-a4f6-e6b40d04b275"],
 " Null wird, und ab diesem Punkt auch alle h\[ODoubleDot]heren Koeffizienten \
verschwinden werden. Je nachdem ob p gerade oder ungerade ist, wird also eine \
der beiden Reihen (die wir ",
 StyleBox["soleven",
  FontFamily->"Source Code Pro"],
 " und ",
 StyleBox["solodd",
  FontFamily->"Source Code Pro"],
 " genannt haben) ein endliches Polynom. Probieren wir das aus (/. ist der \
Substitutionsbefehl):"
}], "Text",
 CellChangeTimes->{{3.8297950098351088`*^9, 3.829795157388875*^9}, {
  3.8299024720572653`*^9, 3.8299025550798073`*^9}, {3.829902614417862*^9, 
  3.829902629706309*^9}},ExpressionUUID->"969d0dec-3b48-4617-9b80-\
96392aaa339a"],

Cell[BoxData[
 RowBox[{"soleven", " ", "/.", " ", 
  RowBox[{"p", "\[Rule]", " ", "2"}]}]], "Input",
 CellChangeTimes->{{3.8297951603693295`*^9, 3.8297951678160267`*^9}, {
  3.829902646784935*^9, 3.8299026469833612`*^9}},
 CellLabel->"In[21]:="],

Cell["\<\
die entsprechende ungerade L\[ODoubleDot]sung fuer p=2 bleibt eine unendliche \
Reihe\
\>", "Text",
 CellChangeTimes->{{3.8297951878252134`*^9, 3.8297952063803425`*^9}, {
  3.8299026634803224`*^9, 3.829902668338818*^9}}],

Cell[BoxData[
 RowBox[{"solodd", " ", "/.", " ", 
  RowBox[{"p", "\[Rule]", " ", "4", " ", 
   RowBox[{"(*", " ", 
    RowBox[{"NB", ":", " ", 
     RowBox[{
     "wir", " ", "haben", " ", "die", " ", "Terme", " ", "nur", " ", "bis", 
      " ", "zur", " ", "9.", " ", "Potenz", " ", 
      RowBox[{"berechnet", "!!"}]}]}], " ", "*)"}]}]}]], "Input",
 CellChangeTimes->{{3.8297951714998813`*^9, 3.8297951769505363`*^9}, {
  3.8297952129524684`*^9, 3.8297952438814683`*^9}},
 CellLabel->"In[22]:="],

Cell["und der umgekehrte Fall:", "Text",
 CellChangeTimes->{{3.829795251710103*^9, 3.8297952581882668`*^9}}],

Cell[BoxData[
 RowBox[{"soleven", " ", "/.", " ", 
  RowBox[{"p", "\[Rule]", "3", " ", 
   RowBox[{"(*", " ", 
    RowBox[{"keiner", " ", "der", " ", "Terme", " ", "verschwindet"}], " ", 
    "*)"}]}]}]], "Input",
 CellChangeTimes->{{3.8297952609877214`*^9, 3.8297952700862064`*^9}, {
  3.8299026899142904`*^9, 3.829902714478965*^9}}],

Cell[BoxData[
 RowBox[{"solodd", " ", "/.", " ", 
  RowBox[{"p", "\[Rule]", " ", "3", " ", 
   RowBox[{"(*", " ", 
    RowBox[{"Potenzen", " ", ">", " ", 
     RowBox[{"3", " ", "fallen", " ", "weg"}]}], " ", "*)"}]}]}]], "Input",
 CellChangeTimes->{{3.8297952735329123`*^9, 3.8297952781867485`*^9}, {
  3.8299027186412926`*^9, 3.8299027334532385`*^9}}],

Cell[TextData[{
 "Die f\[UDoubleDot]r ganzzahlige positive p auftretenden endlichen Polynome \
sind ",
 StyleBox["fast",
  FontSlant->"Italic"],
 " die sogenannten ",
 StyleBox["Legendrepolynome",
  FontSlant->"Italic",
  FontVariations->{"Underline"->True}],
 ", diese geh\[ODoubleDot]ren noch normiert. Die Vorschrift ist einfach, sei ",
 StyleBox["soleven[n,x]",
  FontFamily->"Source Code Pro"],
 " oder ",
 StyleBox["solodd[n,x]",
  FontFamily->"Source Code Pro"],
 " das endliche Polynom fuer n=p, dann ist das ",
 StyleBox["Legendrepolynom",
  FontWeight->"Bold"],
 " als ",
 StyleBox["sol_[n,x]/sol_[n,1]",
  FontFamily->"Source Code Pro"],
 "definiert, der Normierungsfaktor ist also der Wert des entsprechenden \
Polynoms f\[UDoubleDot]r x=1. (",
 StyleBox["sol_",
  FontFamily->"Source Code Pro"],
 " steht fuer ",
 StyleBox["solven",
  FontFamily->"Source Code Pro"],
 " bzw ",
 StyleBox["solodd",
  FontFamily->"Source Code Pro"],
 ", je nachdem ob p gerade oder ungerade ist.\n\nDie folgenden Schritte \
zeigen umst\[ADoubleDot]ndlich (im Sinne der Verwendung von Mathematica), \
aber hoffentlich anschaulich, wie man zu den Legendrepolynomen kommt:\n\n1) \
Gerader Fall, also 0-tes, 2-tes, 4-tes, usw. Legendre Polynom. Wir erstellen \
aus ",
 StyleBox["soleven",
  FontFamily->"Source Code Pro"],
 " eine Tabelle fuer  p=0,2,4,... und dividieren durch den Wert an der Stelle \
x=1, die erreichen wir durch Substitutionsregeln in Mathematica"
}], "Text",
 CellChangeTimes->{{3.8297953652155037`*^9, 3.8297954007822742`*^9}, {
  3.829795436604177*^9, 3.8297954617878013`*^9}, {3.829795497238446*^9, 
  3.829795660512065*^9}, {3.8297958542072773`*^9, 3.829795993726604*^9}, {
  3.8299027392991066`*^9, 3.8299027612759585`*^9}, {3.829902850824428*^9, 
  3.8299028880053673`*^9}, {3.8299029488256855`*^9, 3.829903034109123*^9}}],

Cell[BoxData[
 RowBox[{"evenlegp", "=", 
  RowBox[{"Table", "[", 
   RowBox[{
    RowBox[{
     RowBox[{"(", 
      RowBox[{"soleven", " ", "/.", " ", 
       RowBox[{"p", "\[Rule]", " ", "k"}]}], ")"}], " ", "/", " ", 
     RowBox[{"(", 
      RowBox[{"soleven", " ", "/.", " ", 
       RowBox[{"{", 
        RowBox[{
         RowBox[{"p", "\[Rule]", "k"}], ",", 
         RowBox[{"x", "\[Rule]", "1"}]}], "}"}]}], ")"}]}], ",", " ", 
    RowBox[{"{", 
     RowBox[{"k", ",", "0", ",", "9", ",", "2"}], "}"}]}], "]"}]}]], "Input",
 CellChangeTimes->{{3.829795751027053*^9, 3.829795824432619*^9}, {
  3.8297960083401775`*^9, 3.82979601421529*^9}},
 CellLabel->"In[25]:="],

Cell[BoxData[
 RowBox[{"evenlegp", "//", "TableForm"}]], "Input",
 CellChangeTimes->{{3.8297958300543604`*^9, 3.8297958325902953`*^9}, {
  3.8297960201135483`*^9, 3.82979602297694*^9}},
 CellLabel->"In[26]:="],

Cell["und 2) die ungeraden", "Text",
 CellChangeTimes->{{3.8297960298137426`*^9, 3.8297960550140705`*^9}}],

Cell[BoxData[
 RowBox[{"oddlegp", "=", 
  RowBox[{"Table", "[", 
   RowBox[{
    RowBox[{
     RowBox[{"(", 
      RowBox[{"solodd", " ", "/.", " ", 
       RowBox[{"p", "\[Rule]", " ", "k"}]}], ")"}], " ", "/", " ", 
     RowBox[{"(", 
      RowBox[{"solodd", " ", "/.", " ", 
       RowBox[{"{", 
        RowBox[{
         RowBox[{"p", "\[Rule]", "k"}], ",", 
         RowBox[{"x", "\[Rule]", "1"}]}], "}"}]}], ")"}]}], ",", " ", 
    RowBox[{"{", 
     RowBox[{"k", ",", "1", ",", "9", ",", "2"}], "}"}]}], "]"}]}]], "Input",
 CellChangeTimes->{{3.829795751027053*^9, 3.829795824432619*^9}, {
  3.8297960083401775`*^9, 3.82979601421529*^9}, {3.8297960758098435`*^9, 
  3.8297960881818266`*^9}},
 CellLabel->"In[27]:="],

Cell[BoxData[
 RowBox[{"oddlegp", "//", "TableForm"}]], "Input",
 CellChangeTimes->{{3.8297960922892327`*^9, 3.8297960958096333`*^9}},
 CellLabel->"In[28]:="],

Cell["\<\
Mathematica kennt natuerlich die Legendre Polynome, also z.B. (mit oben \
vergleichen!)\
\>", "Text",
 CellChangeTimes->{{3.829796196900533*^9, 3.8297962152307143`*^9}}],

Cell[BoxData[
 RowBox[{"LegendreP", "[", 
  RowBox[{"2", ",", "x"}], "]"}]], "Input",
 CellChangeTimes->{{3.829796237605343*^9, 3.8297962402986474`*^9}},
 CellLabel->"In[29]:="],

Cell[BoxData[
 RowBox[{"LegendreP", "[", 
  RowBox[{"3", ",", "x"}], "]"}]], "Input",
 CellChangeTimes->{{3.8297962170721383`*^9, 3.82979622216593*^9}},
 CellLabel->"In[30]:="],

Cell["usw.", "Text",
 CellChangeTimes->{{3.8299030721845503`*^9, 3.8299030728221445`*^9}}]
}, Open  ]],

Cell[CellGroupData[{

Cell["Zur \[OpenCurlyQuote]Generating Function\[CloseCurlyQuote]", \
"Subsubsection",
 CellChangeTimes->{{3.8297961423913975`*^9, 3.829796153340789*^9}}],

Cell[TextData[{
 "In der Vorlesung wurde skizziert, wie man die Reihenentwicklung von ",
 Cell[BoxData[
  FormBox[
   SuperscriptBox[
    RowBox[{"(", 
     RowBox[{"1", "+", 
      RowBox[{"t", "^", "2"}], "-", 
      RowBox[{"2", "xt"}]}], ")"}], 
    RowBox[{
     RowBox[{"-", "1"}], "/", "2"}]], TraditionalForm]]],
 " mit u=2xt-t^2 verwenden kann, um die Legendre Polynome zu \
\[OpenCurlyQuote]generieren\[CloseCurlyQuote]. Hier ein paar Hilfestellungen, \
um sich von der Richtigkeit der m\[UDoubleDot]hsamen \
Rechenschritte/Umformungen zu \[UDoubleDot]berzeugen."
}], "Text",
 CellChangeTimes->{{3.829796323141556*^9, 3.829796365021798*^9}, {
  3.8297963975890336`*^9, 3.82979652243381*^9}, {3.829903113303252*^9, 
  3.829903128573887*^9}}],

Cell[BoxData[
 RowBox[{
  RowBox[{"Series", "[", 
   RowBox[{
    RowBox[{"1", "/", 
     RowBox[{"Sqrt", "[", 
      RowBox[{"1", "-", "u"}], "]"}]}], ",", 
    RowBox[{"{", 
     RowBox[{"u", ",", "0", ",", "5"}], "}"}]}], "]"}], " ", 
  RowBox[{"(*", " ", 
   RowBox[{
    RowBox[{"Reihenentwicklung", " ", "um", " ", "u"}], "=", 
    RowBox[{"0", " ", "bis", " ", "zu", " ", "Termen", " ", "in", " ", 
     RowBox[{"u", "^", "5"}]}]}], " ", "*)"}]}]], "Input",
 CellChangeTimes->{{3.8297965393070936`*^9, 3.829796585839329*^9}},
 CellLabel->"In[31]:="],

Cell["\<\
In der Vorlesung wurde das Bildungsgesetz fuer diese Reihe abgeleitet, hier \
als \[OpenCurlyDoubleQuote]Probe\[CloseCurlyDoubleQuote] die Glieder der \
Reihe gem\[ADoubleDot]\[SZ] diesem Bildungsgesetz. (An und f\[UDoubleDot]r \
sich l\[ADoubleDot]sst sich der n=0 Term so nicht ausdr\[UDoubleDot]cken, \
aber Mathematica definiert hilfreicherweise (-1)!! = 1, was jedenfalls in \
diesem Fall auf das Richtige f\[UDoubleDot]hrt:)\
\>", "Text",
 CellChangeTimes->{{3.8297966446255403`*^9, 3.8297966914113655`*^9}, {
  3.8297967746477137`*^9, 3.829796823352643*^9}, {3.829903142452134*^9, 
  3.8299032226799526`*^9}}],

Cell[BoxData[
 RowBox[{"Table", "[", 
  RowBox[{
   RowBox[{
    RowBox[{
     RowBox[{
      RowBox[{"(", 
       RowBox[{
        RowBox[{"2", "n"}], "-", "1"}], ")"}], "!!"}], "/", 
     RowBox[{"(", 
      RowBox[{
       RowBox[{"2", "^", "n"}], " ", 
       RowBox[{"n", "!"}]}], ")"}]}], " ", 
    RowBox[{"u", "^", "n"}]}], ",", 
   RowBox[{"{", 
    RowBox[{"n", ",", "0", ",", "5"}], "}"}]}], "]"}]], "Input",
 CellChangeTimes->{{3.829796718310629*^9, 3.8297967189709144`*^9}},
 CellLabel->"In[32]:="],

Cell["\<\
Nach m\[UDoubleDot]hsamer Rechnerei erhielten wir in der VO eine \
Rechenvorschrift f\[UDoubleDot]r das n-te Legendrepolynom, hier umgesetzt \
fuer Mathematica: (Hinweis: die Floor[] Funtion verwandelt eine reelle Zahl \
in die n\[ADoubleDot]chstkleinere ganze Zahl, Floor[1.0]=1, Floor[2.0]=2, \
Floor[3/2]=1 usw.)\
\>", "Text",
 CellChangeTimes->{{3.829796930483876*^9, 3.8297969791588697`*^9}, {
  3.8297990037167597`*^9, 3.8297990434396935`*^9}, {3.8297990739475193`*^9, 
  3.8297991045930223`*^9}, {3.8299032296787214`*^9, 3.829903258596156*^9}}],

Cell[BoxData[
 RowBox[{
  RowBox[{"Sum", "[", 
   RowBox[{
    RowBox[{
     RowBox[{
      RowBox[{
       RowBox[{"(", 
        RowBox[{
         RowBox[{"2", "n"}], "-", 
         RowBox[{"2", "k"}]}], ")"}], "!"}], "/", 
      RowBox[{"(", 
       RowBox[{
        RowBox[{"2", "^", "n"}], 
        RowBox[{
         RowBox[{"(", 
          RowBox[{"n", "-", "k"}], ")"}], "!"}], 
        RowBox[{"k", "!"}], 
        RowBox[{
         RowBox[{"(", 
          RowBox[{"n", "-", 
           RowBox[{"2", "k"}]}], ")"}], "!"}]}], ")"}]}], 
     RowBox[{
      RowBox[{"(", 
       RowBox[{"-", "1"}], ")"}], "^", "k"}], " ", 
     RowBox[{"x", "^", 
      RowBox[{"(", 
       RowBox[{"n", "-", 
        RowBox[{"2", "k"}]}], ")"}]}]}], ",", 
    RowBox[{"{", 
     RowBox[{"k", ",", "0", ",", 
      RowBox[{"Floor", "[", 
       RowBox[{"n", "/", "2"}], "]"}]}], "}"}]}], "]"}], " ", "/.", " ", 
  RowBox[{"n", "\[Rule]", "4", " ", 
   RowBox[{"(*", " ", 
    RowBox[{"mit", " ", "oben", " ", "vergleichen"}], " ", 
    "*)"}]}]}]], "Input",
 CellChangeTimes->{{3.8297970128670645`*^9, 3.8297970184944267`*^9}},
 CellLabel->"In[33]:="],

Cell["\<\
Darauf aufbauend definieren wir uns eine einfache Funktion, die uns die \
Berechnung beliebieger Legendre Polynome gestattet (Achtung: ineffizient, \
nicht fuer grosse n probieren!)\
\>", "Text",
 CellChangeTimes->{{3.8297970860660577`*^9, 3.8297970971801395`*^9}, {
  3.8297989453550525`*^9, 3.8297989903219576`*^9}}],

Cell[BoxData[
 RowBox[{
  RowBox[{"mylp", "[", 
   RowBox[{"x_", ",", "n_"}], "]"}], ":=", "\[IndentingNewLine]", " ", 
  RowBox[{"Sum", "[", 
   RowBox[{
    RowBox[{
     RowBox[{
      RowBox[{
       RowBox[{"(", 
        RowBox[{
         RowBox[{"2", "n"}], "-", 
         RowBox[{"2", "k"}]}], ")"}], "!"}], "/", 
      RowBox[{"(", 
       RowBox[{
        RowBox[{"2", "^", "n"}], 
        RowBox[{
         RowBox[{"(", 
          RowBox[{"n", "-", "k"}], ")"}], "!"}], 
        RowBox[{"k", "!"}], 
        RowBox[{
         RowBox[{"(", 
          RowBox[{"n", "-", 
           RowBox[{"2", "k"}]}], ")"}], "!"}]}], ")"}]}], 
     RowBox[{
      RowBox[{"(", 
       RowBox[{"-", "1"}], ")"}], "^", "k"}], " ", 
     RowBox[{"x", "^", 
      RowBox[{"(", 
       RowBox[{"n", "-", 
        RowBox[{"2", "k"}]}], ")"}]}]}], ",", 
    RowBox[{"{", 
     RowBox[{"k", ",", "0", ",", 
      RowBox[{"Floor", "[", 
       RowBox[{"n", "/", "2"}], "]"}]}], "}"}]}], "]"}], " "}]], "Input",
 CellChangeTimes->{{3.8297991269901247`*^9, 3.8297991662456675`*^9}},
 CellLabel->"In[34]:="],

Cell[BoxData[
 RowBox[{
  RowBox[{"Table", "[", 
   RowBox[{
    RowBox[{"mylp", "[", 
     RowBox[{"x", ",", "l"}], "]"}], ",", 
    RowBox[{"{", 
     RowBox[{"l", ",", "0", ",", "6"}], "}"}]}], "]"}], "//", "TableForm", 
  " ", 
  RowBox[{"(*", " ", 
   RowBox[{"Test", "/", "Probe"}], " ", "*)"}]}]], "Input",
 CellChangeTimes->{{3.8297991686087255`*^9, 3.82979920290493*^9}},
 CellLabel->"In[35]:="]
}, Open  ]],

Cell[CellGroupData[{

Cell["Generating Function mit Mathematica II", "Subsubsection",
 CellChangeTimes->{{3.8297992434777966`*^9, 3.8297992507685165`*^9}}],

Cell[TextData[{
 "Jetzt das ganze nochmals unter besserer Ausnutzung der \
F\[ADoubleDot]higkeiten von Mathematica: Wir starten wieder mit der \
Reihenentwicklung von ",
 Cell[BoxData[
  FormBox[
   SuperscriptBox[
    RowBox[{"(", 
     RowBox[{"1", "-", "u"}], ")"}], 
    RowBox[{
     RowBox[{"-", "1"}], "/", "2"}]], TraditionalForm]]],
 ", substituieren u=2xt-t^2, und sortieren nach Potenzen von t."
}], "Text",
 CellChangeTimes->{{3.8297992965397887`*^9, 3.829799395492601*^9}, {
  3.8299033195083003`*^9, 3.8299033331605988`*^9}}],

Cell[BoxData[
 RowBox[{
  RowBox[{"Series", "[", 
   RowBox[{
    RowBox[{"1", "/", 
     RowBox[{"Sqrt", "[", 
      RowBox[{"1", "-", "u"}], "]"}]}], ",", 
    RowBox[{"{", 
     RowBox[{"u", ",", "0", ",", "5"}], "}"}]}], "]"}], "  ", 
  RowBox[{"(*", " ", 
   RowBox[{"nochmals", ":", " ", 
    RowBox[{"Mathematica", " ", "Befehl", " ", 
     RowBox[{"(", 
      RowBox[{"u", ".", "a"}], ")"}], " ", "fuer", " ", "Taylorreihen"}]}], 
   " ", "*)"}]}]], "Input",
 CellChangeTimes->{{3.8297994115711174`*^9, 3.829799437871684*^9}},
 CellLabel->"In[36]:="],

Cell[TextData[{
 "Mathematica wei\[SZ], dass die Reihe abgebrochen ist, daher der ",
 Cell[BoxData[
  FormBox[
   SuperscriptBox[
    RowBox[{"O", "[", "u", "]"}], "6"], TraditionalForm]]],
 " Term. Dieser \[OpenCurlyQuote]st\[ODoubleDot]rt\[CloseCurlyQuote] aber im \
folgenden, wir zwingen Mathematica mit einem endlichen Polynom \
weiterzurechnen. NB: Dadurch erhalten wir nur die Legendrepolynome bis zur \
Ordnung 5, h\[ODoubleDot]here Terme sind unvollst\[ADoubleDot]ndig --- bitte \
\[UDoubleDot]berlegen Sie sich, warum!"
}], "Text",
 CellChangeTimes->{{3.82979944881484*^9, 3.8297995540709133`*^9}, {
  3.8299033570774612`*^9, 3.8299033879083014`*^9}}],

Cell[BoxData[
 RowBox[{"s1", "=", 
  RowBox[{"Normal", "[", 
   RowBox[{"Series", "[", 
    RowBox[{
     RowBox[{"1", "/", 
      RowBox[{"Sqrt", "[", 
       RowBox[{"1", "-", "u"}], "]"}]}], ",", 
     RowBox[{"{", 
      RowBox[{"u", ",", "0", ",", "5"}], "}"}]}], "]"}], " ", "]"}], " ", 
  RowBox[{"(*", " ", 
   RowBox[{"Wir", " ", "brechen", " ", "hart", " ", "nach", " ", 
    RowBox[{"u", "^", "5"}], " ", 
    RowBox[{"ab", "!"}]}], "*)"}]}]], "Input",
 CellChangeTimes->{{3.8297995635156746`*^9, 3.8297995930279617`*^9}},
 CellLabel->"In[37]:="],

Cell[BoxData[
 RowBox[{"s2", "=", 
  RowBox[{"s1", " ", "/.", " ", 
   RowBox[{"u", "\[Rule]", " ", 
    RowBox[{
     RowBox[{"2", "x", " ", "t"}], " ", "-", " ", 
     RowBox[{"t", "^", "2"}]}]}]}]}]], "Input",
 CellLabel->"In[38]:="],

Cell[TextData[{
 "Und jetzt die \[OpenCurlyQuote]Magie\[CloseCurlyQuote] (der ",
 StyleBox["Collect[]",
  FontFamily->"Source Code Pro"],
 " Befehl): Wir sortieren nach Potenzen von t, jeder der Faktoren wird dann \
nach Potenzen von x sortiert und vereinfacht. Die Faktoren von ",
 Cell[BoxData[
  FormBox[
   SuperscriptBox["t", "n"], TraditionalForm]]],
 " , n <= 5 sind die Legendrepolynome!"
}], "Text",
 CellChangeTimes->{{3.8297996337525854`*^9, 3.829799736846941*^9}, {
  3.8299034112563667`*^9, 3.8299034121866207`*^9}, {3.8299034455918627`*^9, 
  3.8299034466355357`*^9}}],

Cell[BoxData[
 RowBox[{"Collect", "[", 
  RowBox[{"s2", ",", 
   RowBox[{"{", 
    RowBox[{"t", ",", "x"}], "}"}], ",", "Simplify"}], "]"}]], "Input",
 CellLabel->"In[39]:="],

Cell["Wie gewarnt fehlen fuer n>5 Terme!!", "Text",
 CellChangeTimes->{{3.829799749597209*^9, 3.8297997527769156`*^9}, {
  3.829799841819871*^9, 3.8297998500993767`*^9}, {3.829903455256441*^9, 
  3.8299034636934986`*^9}}]
}, Open  ]],

Cell[CellGroupData[{

Cell["Frobenius mit Mathematica", "Subsubsection",
 CellChangeTimes->{{3.8297998563998537`*^9, 3.829799860809548*^9}}],

Cell["\<\
Seit kurzem kann Mathematica auch die L\[ODoubleDot]sung von DGLs mit der \
Frobeniusmethode berechnen, man bekommt dann tats\[ADoubleDot]chlich die \
Reihen, die man auch \[OpenCurlyQuote]h\[ADoubleDot]ndisch\[CloseCurlyQuote] \
erh\[ADoubleDot]lt.

Hier mal gleich auf die Legendresche DGL angewandt:\
\>", "Text",
 CellChangeTimes->{{3.8297998654594774`*^9, 3.8297999354050555`*^9}, {
  3.829800046020026*^9, 3.829800058014906*^9}, {3.8299034678912535`*^9, 
  3.8299034870430827`*^9}}],

Cell[BoxData[
 RowBox[{"sol", "=", 
  RowBox[{"AsymptoticDSolveValue", "[", " ", 
   RowBox[{
    RowBox[{
     RowBox[{
      RowBox[{
       RowBox[{"(", 
        RowBox[{"1", "-", 
         RowBox[{"x", "^", "2"}]}], ")"}], 
       RowBox[{
        RowBox[{"y", "''"}], "[", "x", "]"}]}], "-", 
      RowBox[{"2", "x", " ", 
       RowBox[{
        RowBox[{"y", "'"}], "[", "x", "]"}]}], "+", 
      RowBox[{"p", "*", 
       RowBox[{"(", 
        RowBox[{"p", "+", "1"}], ")"}], "*", " ", 
       RowBox[{"y", "[", "x", "]"}]}]}], "\[Equal]", "0"}], ",", 
    RowBox[{"y", "[", "x", "]"}], ",", 
    RowBox[{"{", 
     RowBox[{"x", ",", "0", ",", "6"}], "}"}]}], "]"}]}]], "Input",
 CellLabel->"In[40]:="],

Cell["\<\
Man kann klar die beiden Reihen mit geraden bzw. ungeraden Potenzen erkennen, \
allerdings sind die Faktoren nicht optimal zusammengefasst. Falls man dies w\
\[UDoubleDot]nscht, kann man Collect[] zu Hilfe nehmen:\
\>", "Text",
 CellChangeTimes->{{3.829800084328555*^9, 3.82980010085797*^9}, {
  3.829800133095882*^9, 3.8298001787528477`*^9}, {3.8299035026530547`*^9, 
  3.829903513750065*^9}}],

Cell[BoxData[
 RowBox[{"Collect", "[", 
  RowBox[{"sol", ",", 
   RowBox[{"{", 
    RowBox[{
     TemplateBox[{"_"},
      "C"], ",", "x"}], "}"}], ",", "Expand"}], "]"}]], "Input",
 CellLabel->"In[41]:="],

Cell[TextData[{
 "F\[UDoubleDot]r ein ganzzahliges p, z.B. p=3, kann man \
nat\[UDoubleDot]rlich sofort vereinfachen, das hinter C2 verbleibende Polynom \
ist das dritte Legendre Polynom, die Reihe mit geraden Potenzen von x hinter \
C1 bleibt unendlich. In der Praxis ist sie trotzdem abgebrochen, da wir die \
Reihenl\[ODoubleDot]sung nur bis zu Potenzen von ",
 Cell[BoxData[
  FormBox[
   SuperscriptBox["x", "6"], TraditionalForm]]],
 " verlangt haben."
}], "Text",
 CellChangeTimes->{{3.829800243808403*^9, 3.8298003249145737`*^9}, {
  3.829903520144184*^9, 3.8299036900049157`*^9}}],

Cell[BoxData[
 RowBox[{"sol", " ", "/.", " ", 
  RowBox[{"p", "\[Rule]", " ", "3"}]}]], "Input",
 CellChangeTimes->{{3.8298002073427086`*^9, 3.82980021591083*^9}},
 CellLabel->"In[42]:="],

Cell["Und zur Demonstration auch die Airysche DGL:", "Text",
 CellChangeTimes->{{3.8298003444139547`*^9, 3.829800353504196*^9}}],

Cell[BoxData[
 RowBox[{"AsymptoticDSolveValue", "[", 
  RowBox[{
   RowBox[{
    RowBox[{
     RowBox[{
      RowBox[{"y", "''"}], "[", "x", "]"}], "-", 
     RowBox[{"x", " ", 
      RowBox[{"y", "[", "x", "]"}]}]}], "\[Equal]", "0"}], ",", 
   RowBox[{"y", "[", "x", "]"}], ",", 
   RowBox[{"{", 
    RowBox[{"x", ",", "0", ",", "10"}], "}"}]}], "]"}]], "Input",
 CellChangeTimes->{{3.829800366047799*^9, 3.829800381918844*^9}},
 CellLabel->"In[43]:="]
}, Open  ]]
}, Open  ]]
},
WindowSize->{572.25, 459.375},
WindowMargins->{{Automatic, 0}, {Automatic, 0}},
PrivateNotebookOptions->{"CloudPublishPath"->"/Published/legendre_polynomials.\
nb"},
FrontEndVersion->"12.3 for Microsoft Windows (64-bit) (May 11, 2021)",
StyleDefinitions->"Default.nb",
ExpressionUUID->"76d10e14-c8a3-44f5-a8bb-3da206a9e2ee"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[CellGroupData[{
Cell[580, 22, 126, 1, 41, "Subsection",ExpressionUUID->"df190b0c-cf35-4483-86f1-f4dd1b49bb1a"],
Cell[709, 25, 173, 2, 27, "Text",ExpressionUUID->"fec1fdbd-7e12-4491-a7ca-b0673cf80e5d"],
Cell[885, 29, 639, 20, 21, "Input",ExpressionUUID->"bd0286c1-4d44-4c9c-a8da-10a115823fca"],
Cell[1527, 51, 1044, 18, 136, "Text",ExpressionUUID->"537b80e5-6ca4-4afc-a4f0-9a83bd129f86"],
Cell[CellGroupData[{
Cell[2596, 73, 126, 1, 34, "Subsubsection",ExpressionUUID->"13bb59fe-02dc-48e4-8bbf-22eebdfc9d1d"],
Cell[2725, 76, 1011, 30, 45, "Text",ExpressionUUID->"0be05180-c937-4108-a6a6-a5d6e3920908"],
Cell[3739, 108, 1252, 36, 36, "Input",ExpressionUUID->"9c7190b2-c440-4c19-95d6-f34390c77ebb"],
Cell[4994, 146, 936, 22, 100, "Text",ExpressionUUID->"261bfa46-5038-4aae-b983-b8fa3f97056d"],
Cell[5933, 170, 168, 4, 21, "Input",ExpressionUUID->"446b2e3b-b62f-4619-84b6-46fce1e18cbb"],
Cell[6104, 176, 170, 4, 21, "Input",ExpressionUUID->"1a0a8f0f-703c-415a-ab2e-558ea5e9cbb7"],
Cell[6277, 182, 309, 8, 21, "Input",ExpressionUUID->"9b219d43-035a-4608-b9ad-bf013c02e5ac"],
Cell[6589, 192, 238, 7, 36, "Input",ExpressionUUID->"14325149-83d4-48e5-b47a-85cd1e3fe1a5"],
Cell[6830, 201, 657, 18, 36, "Input",ExpressionUUID->"92c226a2-36c6-4d33-a496-259b541cde8a"],
Cell[7490, 221, 656, 18, 36, "Input",ExpressionUUID->"a5029abf-d7e6-49fd-9271-50065f87416f"],
Cell[8149, 241, 1145, 25, 118, "Text",ExpressionUUID->"969d0dec-3b48-4617-9b80-96392aaa339a"],
Cell[9297, 268, 245, 5, 21, "Input",ExpressionUUID->"4ca2394d-c0bc-42d5-accc-5d49a3a366ae"],
Cell[9545, 275, 230, 5, 27, "Text",ExpressionUUID->"e0a29f9a-4c01-4cc7-b180-562ff19301e0"],
Cell[9778, 282, 497, 11, 21, "Input",ExpressionUUID->"37e143e8-4435-46e9-94cf-26da0291d80b"],
Cell[10278, 295, 108, 1, 27, "Text",ExpressionUUID->"8ff83c7c-6b3a-456b-9491-ff2cff41d1f0"],
Cell[10389, 298, 334, 7, 21, "Input",ExpressionUUID->"a75bbca4-a296-4e5f-99cc-6e2f35e42a28"],
Cell[10726, 307, 353, 7, 21, "Input",ExpressionUUID->"ae3d5142-b576-4cdf-97a0-7ec8897a71a7"],
Cell[11082, 316, 1839, 45, 228, "Text",ExpressionUUID->"a1db33d4-0fc2-4e0c-9e6f-11d4f907d483"],
Cell[12924, 363, 671, 18, 21, "Input",ExpressionUUID->"32fabba3-3a30-4f66-82e4-7ef1011e896c"],
Cell[13598, 383, 209, 4, 21, "Input",ExpressionUUID->"c96cfb28-171d-4870-a749-0fe23198cec1"],
Cell[13810, 389, 106, 1, 27, "Text",ExpressionUUID->"47ab7932-35e1-417f-a48f-a4e99b89d99d"],
Cell[13919, 392, 721, 19, 21, "Input",ExpressionUUID->"2796d5ca-3374-40f1-9fb2-bc58ab79727b"],
Cell[14643, 413, 158, 3, 21, "Input",ExpressionUUID->"98e9ea6c-2677-458f-bf32-c0df874dc90f"],
Cell[14804, 418, 179, 4, 27, "Text",ExpressionUUID->"1c4485d7-b417-4ad4-886c-cf4d5667195b"],
Cell[14986, 424, 177, 4, 21, "Input",ExpressionUUID->"26b0fc71-3ca2-4610-9e1c-ddc392346e4e"],
Cell[15166, 430, 176, 4, 21, "Input",ExpressionUUID->"2ef2bda4-4293-4c94-b47a-1d80e860693c"],
Cell[15345, 436, 90, 1, 27, "Text",ExpressionUUID->"794644d2-85fc-4435-8a18-8d4fadb4c619"]
}, Open  ]],
Cell[CellGroupData[{
Cell[15472, 442, 153, 2, 34, "Subsubsection",ExpressionUUID->"df83c99e-ba91-4421-a249-74469fc07d5e"],
Cell[15628, 446, 750, 18, 63, "Text",ExpressionUUID->"e672c01f-5948-429d-915a-1c2564edbbfa"],
Cell[16381, 466, 556, 15, 36, "Input",ExpressionUUID->"9c64c70c-94ec-463f-971f-3d50ac0d7e36"],
Cell[16940, 483, 626, 10, 82, "Text",ExpressionUUID->"57d2e215-99a2-4c67-9a01-b81cff172452"],
Cell[17569, 495, 511, 17, 21, "Input",ExpressionUUID->"9c98910f-6cf7-4329-9958-3b4a808b1c76"],
Cell[18083, 514, 561, 9, 63, "Text",ExpressionUUID->"cfdb38c3-deef-48e5-8c8c-ce39072de4c9"],
Cell[18647, 525, 1139, 38, 36, "Input",ExpressionUUID->"f7a25ba3-bbf9-471c-bcca-6d3c532721df"],
Cell[19789, 565, 328, 6, 45, "Text",ExpressionUUID->"864afdd0-4bae-4e02-980b-e992dfca170a"],
Cell[20120, 573, 1089, 36, 37, "Input",ExpressionUUID->"d862c53f-8cfa-4092-ba57-97d951b8f998"],
Cell[21212, 611, 404, 12, 21, "Input",ExpressionUUID->"4e9c4245-b17b-4466-862c-bb540af78d9f"]
}, Open  ]],
Cell[CellGroupData[{
Cell[21653, 628, 133, 1, 34, "Subsubsection",ExpressionUUID->"c11ea8d7-27d3-42f5-9402-4a9f725b564e"],
Cell[21789, 631, 539, 14, 70, "Text",ExpressionUUID->"084d6444-37cb-4532-9e21-74b8cf8e5c8a"],
Cell[22331, 647, 558, 16, 70, "Input",ExpressionUUID->"9c9ed67e-4429-47c0-9d8d-663d24219bf1"],
Cell[22892, 665, 661, 13, 70, "Text",ExpressionUUID->"9a15780c-8a33-42f4-bde9-99ff0e15d800"],
Cell[23556, 680, 557, 15, 70, "Input",ExpressionUUID->"363d151b-d687-434f-b923-5c52dc6255dd"],
Cell[24116, 697, 236, 7, 70, "Input",ExpressionUUID->"3b85863a-44a9-41c4-bcea-1e3f420882f1"],
Cell[24355, 706, 582, 13, 70, "Text",ExpressionUUID->"4f8f8153-6332-4e7c-a942-48ce3787d805"],
Cell[24940, 721, 174, 5, 70, "Input",ExpressionUUID->"de5b5c33-4d75-4c01-873d-effb085db3a1"],
Cell[25117, 728, 221, 3, 70, "Text",ExpressionUUID->"70ec1e59-14ee-4010-b996-4f7984475da2"]
}, Open  ]],
Cell[CellGroupData[{
Cell[25375, 736, 118, 1, 70, "Subsubsection",ExpressionUUID->"d65f2eb2-17dc-4639-98ac-70194f37f118"],
Cell[25496, 739, 498, 10, 70, "Text",ExpressionUUID->"eb52f624-4d32-41e0-a8fe-26ad8f036e58"],
Cell[25997, 751, 709, 22, 70, "Input",ExpressionUUID->"6ea3ac53-f749-45a7-a22f-bf51f3a648f4"],
Cell[26709, 775, 404, 7, 70, "Text",ExpressionUUID->"53c52d1c-0e06-4291-a0d1-bd4ca9600970"],
Cell[27116, 784, 205, 7, 70, "Input",ExpressionUUID->"183eefc3-9320-456f-a3cc-09979725f8d8"],
Cell[27324, 793, 589, 12, 70, "Text",ExpressionUUID->"6509ea1f-50c2-464c-8015-2b232518a2e4"],
Cell[27916, 807, 187, 4, 70, "Input",ExpressionUUID->"1e6b68a9-86f9-4a55-8516-45a657b736a7"],
Cell[28106, 813, 128, 1, 70, "Text",ExpressionUUID->"7a8ab55b-d771-45d8-95a6-a22ef3509001"],
Cell[28237, 816, 454, 13, 70, "Input",ExpressionUUID->"119c2b07-a84b-4081-a81c-635ec6fc48fd"]
}, Open  ]]
}, Open  ]]
}
]
*)

