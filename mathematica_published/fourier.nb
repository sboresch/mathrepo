(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 12.3' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       158,          7]
NotebookDataLength[      9429,        261]
NotebookOptionsPosition[      7891,        234]
NotebookOutlinePosition[      8362,        251]
CellTagsIndexPosition[      8319,        248]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{
Cell["\<\
Ein einfaches Beispiel, dass die Entwicklung von Fourierreihen mit \
Mathematica illustriert: 

In der Vorlesung wurde die Fourierreihe der Stufenfunktion (f(x)=0, \
-\[Pi]<x<0, f(x)=1, 0<x<2\[Pi]) berechnet. Hier einmal die Funktion, zun\
\[ADoubleDot]chst einmal rekursiv definiert:\
\>", "Text",
 CellChangeTimes->{{3.8315364781256485`*^9, 3.8315365073345394`*^9}, {
  3.8315365714292707`*^9, 3.8315366153970127`*^9}, {3.831536662758481*^9, 
  3.8315367206004763`*^9}, {3.831537018991583*^9, 3.831537047510443*^9}}],

Cell[BoxData[
 RowBox[{
  RowBox[{"fsquare", "[", "x_", "]"}], ":=", " ", 
  RowBox[{"Which", "[", 
   RowBox[{
    RowBox[{"x", ">=", "Pi"}], ",", " ", 
    RowBox[{"fsquare", "[", 
     RowBox[{"x", "-", 
      RowBox[{"2", "*", "Pi"}]}], "]"}], ",", "\[IndentingNewLine]", 
    RowBox[{"x", "<", 
     RowBox[{"-", "Pi"}]}], ",", " ", 
    RowBox[{"fsquare", "[", 
     RowBox[{"x", "+", 
      RowBox[{"2", "*", "Pi"}]}], "]"}], ",", " ", 
    RowBox[{"(*", " ", 
     RowBox[{
     "these", " ", "two", " ", "lines", " ", "take", " ", "care", " ", "of", 
      " ", 
      RowBox[{"periodicity", "!"}]}], " ", "*)"}], "\[IndentingNewLine]", 
    RowBox[{
     RowBox[{"-", "Pi"}], " ", "<=", "x", "<", "0"}], ",", " ", "0", ",", 
    "\[IndentingNewLine]", 
    RowBox[{"0", "<=", "x", "<", "Pi"}], ",", "1"}], "]"}]}]], "Input",
 CellChangeTimes->{{3.8315367257164106`*^9, 3.8315367537623873`*^9}, {
  3.8315367867133617`*^9, 3.8315368423379507`*^9}, {3.8315370699700594`*^9, 
  3.831537169478907*^9}, {3.831537471149005*^9, 3.831537472313222*^9}, {
  3.831537522640253*^9, 3.8315375233268833`*^9}},
 CellLabel->"In[40]:="],

Cell[BoxData[
 RowBox[{"Plot", "[", 
  RowBox[{
   RowBox[{"fsquare", "[", "x", "]"}], ",", 
   RowBox[{"{", 
    RowBox[{"x", ",", 
     RowBox[{
      RowBox[{"-", "3"}], "Pi"}], ",", 
     RowBox[{"3", "Pi"}]}], "}"}], ",", 
   RowBox[{"Exclusions", "\[Rule]", "None"}]}], "]"}]], "Input",
 CellChangeTimes->{{3.831537202318042*^9, 3.831537229379832*^9}},
 CellLabel->"In[42]:="],

Cell["\<\
Mathematica hat auch eine eingebaute Funktion, das folgende Kommando ist \
\[ADoubleDot]quivalent dem obigen:\
\>", "Text",
 CellChangeTimes->{{3.831537254736767*^9, 3.831537269682689*^9}, {
  3.831537320558385*^9, 3.8315373428475323`*^9}, {3.831537552847992*^9, 
  3.831537569975232*^9}}],

Cell[BoxData[
 RowBox[{"Plot", "[", 
  RowBox[{
   RowBox[{"SquareWave", "[", 
    RowBox[{
     RowBox[{"{", 
      RowBox[{"0", ",", "1"}], "}"}], ",", 
     RowBox[{"x", "/", 
      RowBox[{"(", 
       RowBox[{"2", "Pi"}], ")"}]}]}], "]"}], ",", 
   RowBox[{"{", 
    RowBox[{"x", ",", 
     RowBox[{
      RowBox[{"-", "3"}], "Pi"}], ",", 
     RowBox[{"3", "Pi"}]}], "}"}], ",", 
   RowBox[{"Exclusions", "\[Rule]", "None"}]}], "]"}]], "Input",
 CellChangeTimes->{{3.8315373479825435`*^9, 3.831537378979237*^9}},
 CellLabel->"In[31]:="],

Cell["Hier zur Erinnerung die ersten Terme der Fourierentwicklung:", "Text",
 CellChangeTimes->{{3.831537928881922*^9, 3.831537943928815*^9}}],

Cell[BoxData[
 RowBox[{"fs1", "=", 
  RowBox[{"FourierTrigSeries", "[", 
   RowBox[{
    RowBox[{"SquareWave", "[", 
     RowBox[{
      RowBox[{"{", 
       RowBox[{"0", ",", "1"}], "}"}], ",", 
      RowBox[{"x", "/", 
       RowBox[{"(", 
        RowBox[{"2", "Pi"}], ")"}]}]}], "]"}], ",", "x", ",", "5"}], 
   "]"}]}]], "Input",
 CellChangeTimes->{{3.831537831274976*^9, 3.8315378833309937`*^9}, {
  3.831538111838909*^9, 3.831538112737666*^9}},
 CellLabel->"In[49]:="],

Cell["\<\
und hier ein Plot der Funktion im Vergleich zu den ersten Termen der \
Entwicklung.\
\>", "Text",
 CellChangeTimes->{{3.831537992154768*^9, 3.8315380180148387`*^9}}],

Cell[BoxData[
 RowBox[{"Plot", "[", 
  RowBox[{
   RowBox[{"{", 
    RowBox[{
     RowBox[{"fsquare", "[", "x", "]"}], ",", 
     RowBox[{"1", "/", "2"}], ",", 
     RowBox[{
      RowBox[{"1", "/", "2"}], "+", 
      RowBox[{
       RowBox[{"2", "/", "Pi"}], " ", 
       RowBox[{"Sin", "[", "x", "]"}]}]}], ",", 
     RowBox[{
      RowBox[{"1", "/", "2"}], "+", 
      RowBox[{
       RowBox[{"2", "/", "Pi"}], " ", 
       RowBox[{"Sin", "[", "x", "]"}]}], "+", 
      RowBox[{
       RowBox[{"2", "/", " ", 
        RowBox[{"(", 
         RowBox[{"3", " ", "Pi"}], ")"}]}], " ", 
       RowBox[{"Sin", "[", 
        RowBox[{"3", "x"}], "]"}]}]}]}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"x", ",", 
     RowBox[{"-", "Pi"}], ",", "Pi"}], "}"}], ",", 
   RowBox[{"Exclusions", "\[Rule]", "None"}], ",", 
   RowBox[{"PlotLegends", "\[Rule]", "\"\<Expressions\>\""}]}], 
  "]"}]], "Input",
 CellChangeTimes->{{3.83111017199527*^9, 3.8311102530914536`*^9}, {
  3.831537597808015*^9, 3.8315376259162173`*^9}, {3.8315377748262854`*^9, 
  3.831537810523881*^9}, {3.8315379552804785`*^9, 3.83153797827575*^9}},
 CellLabel->"In[47]:="],

Cell["\<\
Und hier jetzt mit ein paar \[OpenCurlyDoubleQuote]Extra-Termen\
\[CloseCurlyDoubleQuote] (Output unterdr\[UDoubleDot]ckt!)\
\>", "Text",
 CellChangeTimes->{{3.8315383527802677`*^9, 3.8315383696461573`*^9}, {
  3.8315384138519745`*^9, 3.831538422811804*^9}}],

Cell[BoxData[
 RowBox[{
  RowBox[{"fs2", "=", 
   RowBox[{"FourierTrigSeries", "[", 
    RowBox[{
     RowBox[{"SquareWave", "[", 
      RowBox[{
       RowBox[{"{", 
        RowBox[{"0", ",", "1"}], "}"}], ",", 
       RowBox[{"x", "/", 
        RowBox[{"(", 
         RowBox[{"2", "Pi"}], ")"}]}]}], "]"}], ",", "x", ",", "25"}], 
    "]"}]}], ";"}]], "Input",
 CellChangeTimes->{{3.8311989555599113`*^9, 3.831198983690545*^9}, {
  3.831199081729455*^9, 3.8311990842820244`*^9}, {3.8312008794475226`*^9, 
  3.8312008866174335`*^9}},
 CellLabel->"In[48]:="],

Cell["\<\
Vergleichsplot Originalfunktion, Entwicklung bis Ordnung 5 und bis Ordnung 25\
\
\>", "Text",
 CellChangeTimes->{{3.8315383758180513`*^9, 3.8315383996087685`*^9}}],

Cell[BoxData[
 RowBox[{"pfp2", "=", 
  RowBox[{"Plot", "[", 
   RowBox[{
    RowBox[{"{", 
     RowBox[{
      RowBox[{"SquareWave", "[", 
       RowBox[{
        RowBox[{"{", 
         RowBox[{"0", ",", "1"}], "}"}], ",", 
        RowBox[{"x", "/", 
         RowBox[{"(", 
          RowBox[{"2", "Pi"}], ")"}]}]}], "]"}], ",", "fs1", ",", "fs2"}], 
     "}"}], ",", 
    RowBox[{"{", 
     RowBox[{"x", ",", 
      RowBox[{
       RowBox[{"-", "3"}], "Pi"}], ",", 
      RowBox[{"3", "Pi"}]}], "}"}], ",", " ", 
    RowBox[{"Exclusions", "\[Rule]", "None"}], ",", 
    RowBox[{"Ticks", "->", 
     RowBox[{"{", 
      RowBox[{
       RowBox[{"Range", "[", 
        RowBox[{
         RowBox[{
          RowBox[{"-", "3"}], "Pi"}], ",", 
         RowBox[{"3", "Pi"}], ",", "Pi"}], "]"}], ",", "Automatic"}], "}"}]}],
     ",", 
    RowBox[{"LabelStyle", "->", 
     RowBox[{"{", 
      RowBox[{
       RowBox[{"FontFamily", "->", "\"\<Arial\>\""}], ",", 
       RowBox[{"FontSize", "->", "16"}], ",", "Black"}], "}"}]}]}], 
   "]"}]}]], "Input",
 CellChangeTimes->{{3.8311990864695454`*^9, 3.831199096280012*^9}, {
  3.831199152193692*^9, 3.8311991843566213`*^9}, {3.8311992929897594`*^9, 
  3.831199301304675*^9}, {3.831200895238834*^9, 3.8312009086939163`*^9}, {
  3.8312009910217547`*^9, 3.831200998219194*^9}, {3.831201107060128*^9, 
  3.8312011262885256`*^9}, {3.831538118802764*^9, 3.8315381489263806`*^9}, {
  3.8315382840218987`*^9, 3.8315383171186976`*^9}},
 CellLabel->"In[54]:="]
},
WindowSize->{694.125, 459.},
WindowMargins->{{Automatic, 0}, {Automatic, 0}},
PrivateNotebookOptions->{"CloudPublishPath"->"/Published/fourier.nb"},
FrontEndVersion->"12.3 for Microsoft Windows (64-bit) (May 11, 2021)",
StyleDefinitions->"Default.nb",
ExpressionUUID->"7665e648-a4cb-410c-a215-e94c3ca8230a"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[558, 20, 528, 10, 82, "Text",ExpressionUUID->"e9423738-feaa-4aa9-9c87-af68fdb9a060"],
Cell[1089, 32, 1129, 27, 65, "Input",ExpressionUUID->"148205c3-4940-41a3-a815-4b8c4843443a"],
Cell[2221, 61, 382, 11, 21, "Input",ExpressionUUID->"2336dd81-e460-40ff-acf5-c569c18726a5"],
Cell[2606, 74, 299, 6, 27, "Text",ExpressionUUID->"cddae6d7-c332-494e-afe9-90aa5f6901ad"],
Cell[2908, 82, 542, 17, 21, "Input",ExpressionUUID->"ef423cc3-694a-4c52-9d00-7e282a02f816"],
Cell[3453, 101, 142, 1, 27, "Text",ExpressionUUID->"3aec51c4-4936-4802-b178-36f7bc0cab09"],
Cell[3598, 104, 474, 14, 21, "Input",ExpressionUUID->"61b34de1-145d-4e29-b540-792344d56288"],
Cell[4075, 120, 175, 4, 27, "Text",ExpressionUUID->"51ae5a5d-49c1-4df8-8d22-5da3d345e03d"],
Cell[4253, 126, 1134, 32, 36, "Input",ExpressionUUID->"1b63890b-25cd-4a2a-9268-c347301026a2"],
Cell[5390, 160, 268, 5, 27, "Text",ExpressionUUID->"744e0924-2f59-4e93-95f0-6d666dae3f9c"],
Cell[5661, 167, 558, 16, 21, "Input",ExpressionUUID->"2c47957b-55b7-404f-b0e6-fc8a482b17cc"],
Cell[6222, 185, 173, 4, 27, "Text",ExpressionUUID->"678e243c-8aaa-4056-84d3-e48fd6b24d1d"],
Cell[6398, 191, 1489, 41, 36, "Input",ExpressionUUID->"d2315c60-1cf8-4d52-b625-9e7ca590ed5d"]
}
]
*)

