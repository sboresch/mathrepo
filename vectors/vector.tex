\documentclass[t,xcolor=dvipsnames,8pt]{beamer}

\usepackage{calc}
\usepackage{tikz}
%\usepackage{pgfplots}
\usepackage{fp}
\usepackage{tabularx}
% This file is a solution template for:

% - Talk at a conference/colloquium.
% - Talk length is about 20min.
% - Style is ornate.



% Copyright 2004 by Till Tantau <tantau@users.sourceforge.net>.
%
% In principle, this file can be redistributed and/or modified under
% the terms of the GNU Public License, version 2.
%
% However, this file is supposed to be a template to be modified
% for your own needs. For this reason, if you use this file as a
% template and not specifically distribute it as part of a another
% package/program, I grant the extra permission to freely copy and
% modify this file as you see fit and even to delete this copyright
% notice. 


\mode<presentation>
{
  \usetheme{default}
%  \setbeamercovered{transparent}
  \setbeamertemplate{navigation symbols}{}
}


\usepackage[english]{babel}
% or whatever

%\usepackage[latin1]{inputenc}
% or whatever

%\usepackage{times}
%\usepackage[T1]{fontenc}
\usepackage{type1cm}
% Or whatever. Note that the encoding and the font should match. If T1
% does not look nice, try deleting the line with the fontenc.

\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{mattens}
%\usepackage{pdfanim}
%\usepackage{multimedia}
%\usepackage{movie15}
\usepackage{alltt}
\usepackage{spot}

\newcommand{\numb}[1]{\multicolumn{1}{r}{$#1\;\;\;\;$}}
\newcommand{\mb}[1]{\ensuremath{\text{\bf #1}}}

\title{Rechnen mit Vektoren}

%\subtitle
%{Include Only If Paper Has a Subtitle}

\author{Stefan Boresch}
% - Give the names in the same order as the appear in the paper.
% - Use the \inst{?} command only if the authors have different
%   affiliation.

\institute{
  Department of Computational Biological Chemistry\\
  Faculty of Chemistry\\
  University of Vienna
}

%\date{December 7, 2006}
\date{December 14, 2020}

\subject{Mathematik f\"ur Molekularbiologen: Vektorrechnung}
% This is only inserted into the PDF information catalog. Can be left
% out. 


% If you have a file called "university-logo-filename.xxx", where xxx
% is a graphic format that can be processed by latex or pdflatex,
% resp., then you can add a logo as follows:

% \pgfdeclareimage[height=0.5cm]{university-logo}{university-logo-filename}
% \logo{\pgfuseimage{university-logo}}

% If you wish to uncover everything in a step-wise fashion, uncomment
% the following command: 

%\beamerdefaultoverlayspecification{<+->}

\begin{document}

\begin{frame}
  \titlepage
\end{frame}

\section{Pr\"aambel}

\begin{frame}
%\frametitle{Lizenz}
\vspace*{1cm}

\begin{center}
Copyright (c) 2008,2020 Stefan Boresch
\end{center}

\vspace*{1cm} Permission is granted to copy, distribute and/or modify
this document under the terms of the
\href{http://www.gnu.org/licenses/fdl.html}{\color{blue}GNU Free Documentation
License, Version 1.2} or any later version published by the Free
Software Foundation; with no Invariant Sections, no Front-Cover Texts,
and no Back-Cover Texts.  A copy of the license is included in the
section entitled "GNU Free Documentation License".
\end{frame}

\section{Einleitung}

\begin{frame}
\frametitle{Vektoren} 

Wir haben uns bereits mit Funktionen mehrerer Ver\"anderlicher
besch\"aftigt.  Eine bestimmte Eigenschaft h\"angt von mehreren
Gr\"o{\ss}en ab, welche von einander unabh\"angig sind. Mit geeigneten Apparaturen kann ich z.B.\ Druck und
Temperatur eines Gases getrennt kontrollieren, das Volumen des Gases
ist eine Funktion dieser beiden Gr\"o{\ss}en (plus nat\"urlich der
Molmenge des vorhandenen Gases!).\\[0.25cm]

\visible<2->{Das Volumen eines Gases kann durch eine Zahl beschrieben werden, der
Wert dieser Zahl h\"angt von mehreren anderen Eigenschaften ab.  Es
gibt jedoch auch Eigenschaften, die sich nicht durch eine Zahl alleine
ausdr\"ucken lassen. Die Angabe ``\alert<2>{\em Ein Auto f\"ahrt mit einer
Geschwindigkeit von 25~km/h}'' allein ist nicht sehr aussagekr\"aftig,
denn wir wissen nicht ``wohin'' dieses Auto f\"ahrt. Derartige
\"Uberlegungen f\"uhren auf den Begriff des \alert<3>{\em Vektors}.}
\end{frame}

\begin{frame}{Vektoren}

  Ein Vektor
ist charakterisiert durch

\begin{itemize}
\item L\"ange ({\em Betrag})
\item Richtung
\item Richtungssinn (Orientierung)
\end{itemize}

Bezogen auf das ``Autobeispiel'' bedeuten diese drei Punkte: (1) Die
L\"ange~/ Betrag sind die 25~km/h. Die Richtung w\"urde
z.B. heissen auf einer Geraden zwischen Wien und Salzburg, w\"ahrend
Richtungssinn (Orientierung) bedeutet, ob von Wien nach Salzburg oder von Salzburg nach
Wien.
\end{frame}

\begin{frame}
\frametitle{Grundeigenschaften von Vektoren}
Um zu verdeutlichen, da{\ss} es sich bei einer Gr\"o{\ss}e {\em a} um einen Vektor
handelt, schreibt man \spot<2>{$\vec a$}. In B\"uchern wird stattdessen h\"aufig Fettdruck genommen, d.h., \spot<3>{$\mb{a}$}. Manchen Leuten ist der Pfeil zu ``m\"uhsam'', daher schreiben sie \spot<4>{$\Sb*{a}$}. Vor allem die \"altere, deutschsprachige
Literatur verwendet Frakturschrift \spot<5>{$\mathfrak{a}$}.\\[0.25cm]

\visible<6->{Vektoren werden \"ublicherweise durch Pfeile dargestellt. Beachten Sie bitte, da{\ss} wie gezeigt eine Parallelverschiebung einen Vektor nicht \"andert, alle gezeigten Pfeile sind daher Darstellungen des Vektors $\vec a$\\[1cm]

\begin{center}
\input{vector_fig6}
\end{center}}



\end{frame}
\begin{frame}
\frametitle{Grundeigenschaften von Vektoren II}
Wir wollen jetzt einige Grundeigenschaften von Vektoren besprechen.
\begin{itemize}
\item Ein wichtiges Charakteristikum eines Vektors ist sein {\em
Betrag}. Im zwei- und dreidimensionalen Raum entspricht der Betrag
eines Vektors der L\"ange des Vektors (anschaulich also der L\"ange
des Pfeils, die den Vektor repr\"asentiert). Wir schreiben f\"ur L\"ange~/ Betrag von $\vec a$ unter Verwendung des Betragszeichen $\left|\vec a\right|$. Man
schreibt auch 
\[\left|\vec a \right|=a\]
\item<2-> Unter Verwendung des L\"angenbegriffs k\"onnen wir jetzt die {\em
skalare Multiplikation} einf\"uhren (Multiplikation eines Vektors $\vec
a$ mit einem Skalar (einer Zahl) $\lambda$): $\lambda\,\vec a$.
Diese l\"a{\ss}t die Richtung unver\"andert, \"andert aber die L\"ange des
Vektors um den Faktor $\lambda$, d.h., f\"ur die L\"ange von $\vec a$ gilt:
\[\left|\lambda\,\vec a\right|=\lambda\,\left|\vec a\right|=\lambda\,a\]
\item<3-> {\em Multiplikation mit einer negativen Zahl} \"andert den {\em Richtungssinn} des Vektors; insbesondere entspricht $-\vec a=(-1)\,\vec a$ einem Vektor mit derselben L\"ange und Richtung wie $\vec a$, aber gegengleichem Richtungssinn (Orientierung)
\end{itemize}

\begin{center}
\input{vector_fig7}
\end{center}

\end{frame}

\begin{frame}
\frametitle{Grundeigenschaften von Vektoren III}
\begin{itemize}
\item {\em Einheitsvektor:} Die drei Grundcharakteristika eines Vektors (Betrag, Richtung, Richtungssinn) k\"onnen in zwei Untergruppen getrennt werden, \spot<1>{Betrag} einerseits, \spot<1>[fill=blue!30]{Richtung} und \spot<1>[fill=blue!30]{Richtungssinn} andererseits (die Unterscheidung zwischen letzeren ist ja zumind. in der Umgangssprache etwas unscharf). \visible<2->{Manchmal ist es n\"utzlich zwischen \spot<2>{Betrag} einerseits und \spot<2>[fill=blue!30]{Richtung} und \spot<2>[fill=blue!30]{Richtungssinn} andererseits zu trennen. Dies erreicht man durch
\[\vec a=\spot<2>{\left|\vec a\right|}\,\spot<2>[fill=blue!30]{\vec a^\circ}\]
d.h., der Vektor wird als skalares Produkt seiner L\"ange mit seinem \spot<3>[fill=blue!30]{Einheitsvektor} $\vec a^\circ$ ausgedr\"uckt.

\item<4->Ein Einheitsvektor $\vec a^\circ$ hat die L\"ange 1, besitzt aber die gleiche Richtung und Orientierung (=Richtungssinn) wie
$\vec a$. Aus einem beliebigen Vektor $\vec a$ bekommt man einen Einheitsvektor durch
\[\vec a^\circ=\frac{1}{\left|\vec a\right |}\vec a\]}
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Addition, Subtraktion}
Die Addition zweier Vektoren kann man sich als ``geometrische Addition gerichteter Strecken'' vorstellen.

\begin{center}
\vspace*{-2.5cm}\hspace*{-2cm}\input{vector_fig8}
\end{center}
\end{frame}

\begin{frame}
\frametitle{Addition, Subtraktion}
Die Addition ist 
\begin{itemize}
\item kommutativ: $\vec a+\vec b=\vec b+\vec a$
\item assoziativ: $(\vec a+\vec b)+\vec c=\vec a+(\vec b+\vec c)$
\item distributiv bez\"uglich der skalaren Multiplikation
\[\lambda(\vec a+\vec b)=\lambda\,\vec a+\lambda\,\vec b\quad\text{bzw.}\]
\[(\lambda+\mu)\vec a=\lambda\,\vec a+\mu\,\vec a\quad(\lambda,\mu\in\mathbb{R})\]
\end{itemize}
Die Subtraktion $\vec a-\vec b$ ergibt sich wie gezeigt durch {\em
Addition} von $-\vec b$ zu $\vec a$. Alternativ erh\"alt man den
Differenzvektor (gr\"uner, gepunkteter Pfeil) indem man $\vec b$ in
den Ausgangspunkt von $\vec a$ parallel verschiebt und einen Vektor
von dieser Pfeilspitze zur Pfeilspitze von $\vec a$ verbindet bildet.
\end{frame}

\section{Koordinatensysteme, Koordinatendarstellung}

\begin{frame}
\frametitle{Darstellung in vektoriellen Komponenten}

\begin{center}
\input{vector_fig1}
\end{center}

Wir gehen jetzt zur Darstellung von Vektoren in \alert<1>{rechtwinkeligen} Koordinatensytemen \"uber (siehe obige Darstellung f\"ur 2D).  \visible<2->{ Wie gezeigt, wurde der Vektor $\vec a$ in zwei Komponenten $\vec a_1$ und $\vec a_2$ zerlegt, die parallel zu den Koordinatenachsen liegen. \visible<3->{Es gilt

\[\vec a=\vec a_1+\vec a_2\]
\visible<4->{\[\left|\vec a_1\right|=a_1=x_2-x_1,\qquad\left|\vec a_2\right|=a_2=y_2-y_1\]
\visible<5->{\[\left|\vec a\right|=a=\sqrt{a_1^2+a_2^2}\]}}}}
\end{frame}

\begin{frame}
\frametitle{Darstellung in vektoriellen Komponenten}
F\"ur den dreidimensionalen Raum gilt eine analoge Zerlegung
\[\vec a=\vec a_1+\vec a_2+\vec a_3,\]
wobei sich die $\vec a_1$, $\vec a_2$, $\vec a_3$ wieder als parallel zu den Koordinatenachsen $x$, $y$, $z$ verstehen. Wenn nicht speziell vermerkt, operieren wir mit einem Rechtssystem (Erkl\"arung in K\"urze!).

\end{frame}

\begin{frame}
\frametitle{Basisvektoren}
\begin{center}
\input{vector_fig2}
\end{center}
Die Abbildung der Vorseite ist jetzt um die zwei Vektoren $\vec e_1$ und $\vec e_2$ erweitert, Einheitsvektoren (L\"ange = 1) parallel zu den beiden Koordinatenachsen. Derartige Vektoren werden als \spot<1>{\em Basisvektoren} (Grundvektoren) bezeichnet. \visible<2->{Weiters gilt
\[\vec a_1=\left|\vec a_1\right|\vec e_1=a_1\vec e_1\qquad \vec a_2=\left|\vec a_2\right|\vec e_2=a_2\vec e_2\]
und somit aber auch
\[\vec a=a_1\vec e_1 + a_2\vec e_2\]
\visible<3->{Da aber in einem rechtwinkeligen Koordinatensystem kein Zweifel \"uber die Natur der Einheitsvektoren besteht, ist ein Vektor $\vec a$ durch die {\em Komponenten} $a_1$, $a_2$ eindeutig bestimmt, und man schreibt
\[\vec a=(a_1,a_2)=\begin{pmatrix}
a_1\\
a_2
\end{pmatrix}\]}}
\end{frame}

\begin{frame}
\frametitle{Basisvektoren II}
Die Darstellung eines Vektors
\[\vec a=(a_1,a_2)=\begin{pmatrix}
a_1\\
a_2
\end{pmatrix}
\quad\text{(in 2D)}\qquad
\vec a=(a_1,a_2,a_3)=\begin{pmatrix}
a_1\\
a_2\\
a_3
\end{pmatrix}
\quad\text{(in 3D)}\]
wird als Komponentendarstellung von $\vec a$ bezeichnet. \visible<2->{Die Basisvektoren
haben Komponenten
\[\vec e_1=(1,0),\;\vec e_2=(0,1)\quad\text{(in 2D)}\]
\[\vec e_1=(1,0,0),\;\vec e_2=(0,1,0),\;\vec e_3=(0,0,1)\quad\text{(in 3D)}\]}
\end{frame}

\begin{frame}
\frametitle{Basisvektoren II}
Aus den gegebenen Definition folgen unmittelbar die Rechenregeln f\"ur skalare Multiplikation und (vektorielle) Addition in Komponentendarstellung (auf den ``Beweis'' wird verzichtet):
\[\lambda\vec a=\lambda\begin{pmatrix}
a_1\\
a_2\\
a_3
\end{pmatrix}=
\begin{pmatrix}
\lambda\,a_1\\
\lambda\,a_2\\
\lambda\,a_3
\end{pmatrix}\]
\[\vec a+\vec b= 
\begin{pmatrix}
a_1\\
a_2\\
a_3
\end{pmatrix}
+\begin{pmatrix}
b_1\\
b_2\\
b_3
\end{pmatrix}=
\begin{pmatrix}
a_1+b_1\\
a_2+b_2\\
a_3+b_3
\end{pmatrix}
\]

\end{frame}

\begin{frame}
\frametitle{Einschub: Richtungskosinus eines Vektors}
\begin{center}
\input{vector_fig3}
\end{center}
{\small Die Gr\"o{\ss}en
\[{\color{red}\cos[\angle(\vec e_1,\vec a)]=\frac{a_1}{\left|\vec a\right|}}\qquad
{\color{blue}\cos[\angle(\vec e_2,\vec a)]=\frac{a_2}{\left|\vec a\right|}}
\]
hei{\ss}en \spot<1>{Richtungskosinus} des Vektors $\vec a$. Dabei sind die
Winkel immer so zu messen, da{\ss} sie zwischen 0 und 180$^\circ$
liegen. \visible<2->{Durch die Angabe der zwei (drei) Richtungskosinus (2D bzw. 3D)
sind Richtung und Orientierung eines Vektors $\vec a$
festgelegt. F\"ur die Richungskosinus gelten folgende Beziehungen
(gezeigt f\"ur 2D, analog in 3D):

\[\cos^2[\angle(\vec e_1,\vec a)]+\cos^2[\angle(\vec e_2,\vec a)]=
\frac{a_1^2}{\left|\vec a\right|^2}+\frac{a_2^2}{\left|\vec a\right|^2}=1
\]
sowie
\[a_1\cos[\angle(\vec e_1,\vec a)]+
a_2\cos[\angle(\vec e_2,\vec a)]=\frac{a_1^2+a_2^2}{\left|\vec a\right|}=\left|\vec a\right|
\]
\visible<3->{Weiters lassen sich die Komponenten eines Vektors via Richtungskosinus ausdr\"ucken:
\[a_1=\left|\vec a\right|\,\cos[\angle(\vec e_1,\vec a)]\qquad
a_2=\left|\vec a\right|\,\cos[\angle(\vec e_2,\vec a)]
\]
}}}
\end{frame}

\section{Skalarprodukt}

\begin{frame}
\frametitle{Skalarprodukt}


In der Praxis haben sich zwei(!) Produkte zwischen zwei Vektoren als n\"utzlich erwiesen. Beide sind letztlich aus Anwendungen der Physik motiviert.  Als erstes  besch\"aftigen wir uns mit dem {\em Skalarprodukt} (auch {\em inneres} Produktgenannt)
\end{frame}

\begin{frame}
\frametitle{Skalarprodukt}

Ausgangspunkt ist die an einem K\"orper geleistete Arbeit, die salopp als
\[\text{Arbeit W = Kraft $\left|\vec F\right|$ mal Weg $\left|\vec s\right|$}\]
definiert ist. \visible<2->{ Dies gilt aber in dieser Form nur, wenn die Richtung und Orientierung von Kraft und Weg \"ubereinstimmen. Betrachten wir den allgemeineren Fall
\begin{center}
\input{vector_fig4}
\end{center}
\alert<2>{Der K\"orper/Massenpunkt k\"onne sich nur in Richtung von $\vec s$
bewegen.} \visible<3->{F\"ur die Arbeit (die an diesem K\"orper geleistet wird) ist
daher nur die Kraftkomponente $\alert{\vec F_x}$, die parallel zu $\vec s$ liegt, von
Bedeutung.}\visible<4->{ Unter Verwendung des Winkels $\angle(\vec s;\vec F)=\alert{\theta}$, gilt dann:
\[W=\left|\vec F_x\right|\,\left|\vec s\right|=\left|\vec F\right|\cos\theta\,\left|\vec s\right|\]
}}
\end{frame}

\begin{frame}
\frametitle{Skalarprodukt II}
Diese und \"ahnliche Anwendungen f\"uhren zur Definition des Skalarprodukts
\[\vec a\cdot \vec b={\color{gray}\vec a\,\vec b=(\vec a\,\vec b)=\langle\vec a\,\vec b\rangle=}\left|\vec a\right|\left|\vec b\right| \cos\theta=a b \cos\theta\]
wobei der von $\vec a$ und $\vec b$ eingeschlossene Winkel $\theta$ so gemessen wird, da{\ss} er zwischen 0 und 180$^\circ$ liegt. In {\color{gray} grau} sind alternative Schreibweisen des Skalarprodukts gezeigt. Mit Hilfe des Skalarprodukts l\"a{\ss}t sich z.B. die Arbeit einfach als
\[W=\vec F\cdot\vec s\]
definieren.\\[0.25cm]
\end{frame}

\begin{frame}
\frametitle{Skalarprodukt II}
           {\bfseries Anmerkungen:}
\begin{enumerate}
\item Das Ergegnis des Skalarprodukts ist eine Zahl (=Skalar).
  \item<2-> Daher,  hat das Skalarprodukt auch keine eindeutige Umkehrung.
\item<3-> Das Skalarprodukt ist kommutativ
\[\vec a\cdot \vec b=\vec b\cdot\vec a,\]
\item<4->  distributiv bez\"uglich Vektoraddition
\[\vec a\cdot(\vec b+\vec c)=\vec a\cdot\vec b+\vec a\cdot\vec b\]
\item<5-> und es gilt:
  \[(\lambda\vec a)\cdot\vec b=\lambda(\vec a\cdot\vec b)=\lambda\,\vec a\cdot\vec b\]
  \end{enumerate}
\end{frame}

\begin{frame}
\frametitle{Skalarprodukt III} Aus den Eigenschaften des Kosinus
ergibt sich, da{\ss} das Skalarprodukt zweier Vektoren verschwindet,
wenn sie einen Winkel von 90$^\circ$ einschlie{\ss}en ($\cos90^\circ=0$). Somit gilt,
da{\ss} {\em zwei Vektoren genau zueinander normal stehen, wenn ihr
Skalarprodukt $=0$ ist}.\\[0.25cm]

\visible<2->{Ein weiterer wichtiger Spezialfall ist 
\[\vec a\cdot\vec a=a\,a\cos 0^\circ=a^2=\left|\vec a\right|^2\]
woraus sich der Betrag eines Vektors auch als
\[\left|\vec a\right|=\sqrt{\vec a\cdot\vec a}\]
definieren l\"a{\ss}t. (Dieses per se triviale Ergebnis hat Konsequenzen f\"ur Vektoren mit komplexen Komponenten!)\\[0.25cm]

\visible<3->{Die Basisvektoren $\vec e_1$, $\vec e_2$ in 2D, bzw.\ $\vec e_1$, $\vec e_2$, $\vec e_3$ in 3D stehen aufeinander normal. Somit gilt z.B
\[\vec e_1\cdot\vec e_1=1,\text{ aber }\vec e_1\cdot \vec e_2=0\]
\visible<4->{Derartige Zusammenh\"ange lassen sich mit Hilfe des sogenannten \alert{Kronecker-Symbols} kompakt schreiben:
\[\vec e_i\cdot\vec e_k=\spot<4>{\delta_{ik}}=\begin{cases} 

1 \text{ f\"ur } i=k \\
0 \text{ f\"ur } i\ne k.
\end{cases}\qquad(*)
\] }}}
\end{frame}

\begin{frame}
\frametitle{Skalarprodukt in Komponentendarstellung}
F\"ur
\[\vec a=a_1\vec e_1+a_2\vec e_2+a_3\vec e_3,\quad \vec b=b_1\vec e_1+b_2\vec e_2+b_3\vec e_3\]
erh\"alt man durch direktes Ausmultiplizieren
\[\vec a\cdot \vec b= \begin{aligned}[t]
a_1b_1\vec e_1\cdot\vec e_1 &\;+ {\color{gray}a_1b_2\vec e_1\cdot\vec e_2} &\;+&\; {\color{gray}a_1b_3\vec e_1\cdot\vec e_3} & + \\
{\color{gray}a_2b_1\vec e_2\cdot\vec e_1} &\;+ a_2b_2\vec e_2\cdot\vec e_2 &\;+&\; {\color{gray}a_2b_3\vec e_2\cdot\vec e_3} & + \\
{\color{gray}a_3b_1\vec e_3\cdot\vec e_1} &\;+ {\color{gray}a_3b_2\vec e_3\cdot\vec e_2} &\;+&\; a_3b_3\vec e_3\cdot\vec e_3 & 
\end{aligned}
 \]
\visible<2->{die grauen Terme fallen wegen (*) weg, und mit $\vec e_i\cdot \vec e_i=1$ findet man die (vermutlich bekannte) Rechenregel:
\[\vec a\cdot \vec b=a_1b_1+a_2b_2+a_3b_3\]

\visible<3->{Zum Abschlu{\ss} ein Beispiel mit Zahlen:
\[\begin{pmatrix}
1\\
3\\
-1
\end{pmatrix}\cdot
\begin{pmatrix}
-2\\
2\\
5
\end{pmatrix}=(1)\,(-2)+(3)\,(2)+(-1)\,(5)=-2+6-5=-1
\]}}
\end{frame}

\section{Vektorprodukt}

\begin{frame}
\frametitle{Rechts- und Linkssystem, Dreifingerregel/Rechte-Hand-Regel}
In 3D sollte man aufpassen, nicht irrt\"umlich mit einem Linkssystem zu operieren
\begin{center}
\begin{tabular}{cc}
Rechtssystem & Linkssystem \\
\input{vector_fig5a} & \input{vector_fig5b} 
\end{tabular}
\end{center}

\visible<2->{Stehen die drei Basisvektoren in $x-$, $y-$ und $y-$Richtung wie Daumen, Zeige- und Mittelfinger der {\em rechten} Hand
\begin{center}
\includegraphics[scale=0.15]{Rechte-hand-regel}
\end{center}
so handelt es sich um ein Rechtssystem.
}
\end{frame}

\begin{frame}
\frametitle{Vektorprodukt}
Die Physik (z.B.\ Drehmoment, Elektromagnetismus) motiviert im $\mathbb{R}^3$ (3D und nur dort!) 
ein zweites Produkt zweier Vektoren, das sog. Vektorprodukt (bzw.\ Kreuzprodukt oder \"au{\ss}eres Produkt)
\visible<2->{\[\vec c=\vec a\times\vec b={\color{gray}[\vec a\,\vec b]}=
\underbrace{\left|\vec a\right|\left|\vec b\right|\sin\theta}_{\left|\vec c\right|}\,\spot<2>{\vec c^\circ}
\]
(Wie beim Skalarprodukt zeigt der {\color{gray} graue Text} eine alternative Schreibweise f\"ur das Vektorprodukt).
$\theta$ ist wieder der von $\vec a$ und $\vec b$ eingeschlossene Winkel ($0\le\theta\le 180^\circ$).
\visible<3->{\alert<3>{Der Resultatsvektor $\vec c$ hat die L\"ange $\left|\vec a\right|\left|\vec b\right|\sin\theta$ und
steht {\em normal} zu $\vec a$ sowie $\vec b$, $\vec a\bot\vec c$,  $\vec b\bot\vec c$. Die Vektoren
$\vec a$, $\vec b$ und $\vec c$ bilden (in dieser Reihenfolge) ein {\em Rechtssystem}.}
\visible<4->{\begin{center}
\includegraphics[scale=0.075]{Crossproduct}
\end{center}}}
}
\end{frame}

\begin{frame}
\frametitle{Vektorprodukt II}
           {\bfseries Anmerkungen:}
\begin{enumerate}

           \item Es folgt aus der Definition, da{\ss} das Vektorprodukt {\em nicht} kommutativ
ist, $\vec a\times\vec b=-\vec b\times \vec a$.
\item<2-> Da der Sinus bei 0$^\circ$ und $180^\circ$ verschwindet, verschwindet das Vektorprodukt zweier
paralleler Vektoren.
\item<3-> Geometrisch kann die L\"ange des Resultatsvektors $\left|\vec c\right|$ als die Fl\"ache des von $\vec a$ und 
$\vec b$ aufgespannten Parallelogramms interpretiert werden.
\item<4-> Auch das Vektorprodukt hat keine eindeutige Umkehrung
\item<5-> Das Vektorprodukt ist auch nicht assoziativ, jedoch
\item<6-> distributiv bez\"uglich der Vektoraddition
\item<7-> Das Vektorprodukt ist nur f\"ur dreidimensionale Vektoren definiert
\end{enumerate}

\end{frame}


\begin{frame}
\frametitle{Vektorprodukt II}
{\bfseries Vektorprodukt der Basisvektoren:} Aus der Definition des Vektorprodukts folgt sofort, da{\ss} das Vektorprodukt eines Basisvektors mit sich selbst Null ist, d.h.

\[\vec e_i\times\vec e_i=0\]

\visible<2->{\vspace*{2ex}F\"ur Kreuzprodukte der Form $\vec e_i\times\vec e_j$, $i\ne j$ mu{\ss} der 
  Ergebnisvektor jedenfalls die L\"ange 1 haben (warum?) und parallel zum jeweils dritten Basisvektor $\vec e_k$, $k\ne i,j$ sein (dieser dritte Vektor ist ja per Definition normal zu den beiden anderen Basisvektoren!).


\visible<3>{\vspace*{3ex}  Nur das Vorzeichen mu{\ss} mit der Dreifingerregel bestimmt werden. Es gilt
\[\vec e_1\times\vec e_2=\vec e_3\qquad
  \vec e_2\times\vec e_3=\vec e_1\qquad
  \vec e_3\times\vec e_1=\vec e_2\]
\[\vec e_2\times\vec e_1=-\vec e_3\qquad
  \vec e_3\times\vec e_2=-\vec e_1\qquad
  \vec e_1\times\vec e_3=-\vec e_2\]
}}
\end{frame}

\begin{frame}
\frametitle{Komponentendarstellung des vektoriellen Produkts}

F\"ur
\[\vec a=a_1\vec e_1+a_2\vec e_2+a_3\vec e_3,\quad \vec b=b_1\vec e_1+b_2\vec e_2+b_3\vec e_3\]
erh\"alt man durch direktes Ausmultiplizieren
\visible<2->{\[\vec a\times \vec b= \begin{aligned}[t]
{\color{gray}a_1b_1\vec e_1\times\vec e_1} &\;+ a_1b_2\vec e_1\times\vec e_2 &\;+&\; a_1b_3\vec e_1\times\vec e_3 & + \\
a_2b_1\vec e_2\times\vec e_1 &\;+ {\color{gray}a_2b_2\vec e_2\times\vec e_2} &\;+&\; a_2b_3\vec e_2\times\vec e_3 & + \\
a_3b_1\vec e_3\times\vec e_1 &\;+ a_3b_2\vec e_3\times\vec e_2 &\;+&\; {\color{gray}a_3b_3\vec e_3\times\vec e_3} & 
\end{aligned}
 \]
\visible<3->{Wegen der eben gezeigten Rechenregeln f\"ur Kreuzprodukte der Basikvektoren
fallen 
die grauen Terme weg, und die anderen Terme lassen sich vereinfachen.
\[\vec a\times \vec b= \begin{aligned}[t]
{\color{white}a_1b_1\vec e_1\times\vec e_1} &\;+ a_1b_2\overbrace{\vec e_1\times\vec e_2}^{\alert{\vec e_3}} &\;+&\; a_1b_3\vec e_1\times\vec e_3 & + \\
a_2b_1\vec e_2\times\vec e_1 &\;+ {\color{white}a_2b_2\vec e_2\times\vec e_2} &\;+&\; a_2b_3\vec e_2\times\vec e_3 & + \\
a_3b_1\vec e_3\times\vec e_1 &\;+ a_3b_2\underbrace{\vec e_3\times\vec e_2 }_{\alert{-\vec e_1}}&\;{\color{white}+}&\; {\color{white}a_3b_3\vec e_3\times\vec e_3} & 
\end{aligned}
\]
usw.\\[1ex]


\visible<4->{Somit findet man:
\[\vec a\times \vec b=
(a_2b_3-a_3b_2)\vec e_1+
(a_3b_1-a_1b_3)\vec e_2+
(a_1b_2-a_2b_1)\vec e_3
\]}}}

\end{frame}

\begin{frame}
\frametitle{Komponentendarstellung des vektoriellen Produkts}
Der folgende Trick hilft als {\bfseries Merkregel}: Man schreibt wie gezeigt die $x$-Komponenten der beiden Vektoren unter der $z$-Komponente nochmals an.\visible<2->{ F\"ur die $x$-Komponente des Kreuzprodukts sind nur die $y$- und $z$-Komponenten der Ausgangsvektoren von Interesse, wobei das Produkt der {\color{green!50!black}gr\"unmarkierten} Komponenten vom Produkt der {\color{red}roten} Kompoenten abzuziehen ist.}\visible<3->{ Man wiederholt dies sinngem\"a{\ss} f\"ur die $y$- und $z$-Komponente.}\\

\only<1>{\[
\begin{array}{ccccc}
\left (
\begin{array}{c}
a_1 \\
a_2 \\
a_3 
\end{array}
\right ) &
\times &
\left (
\begin{array}{c}
b_1 \\
b_2 \\
b_3 
\end{array}
\right ) &
= &
\begin{pmatrix}
{\color{white}a_2b_3}{\color{white}-a_3b_2}\\
{\color{white}a_3b_1}{\color{white}-a_1b_3}\\
{\color{white}a_1b_2}{\color{white}-a_2b_1}
\end{pmatrix}
\\
\color{blue}a_1 & & \color{blue}b_1 & & \\
\end{array}
\]}
%
\only<2>{\[
\begin{array}{ccccc}
\left (
\begin{array}{c}
{\color{gray}a_1} \\
{\color{red}a_2} \\
{\color{green!50!black}a_3 }
\end{array}
\right ) &
\times &
\left (
\begin{array}{c}
{\color{gray}b_1} \\
{\color{green!50!black}b_2 } \\
{\color{red}b_3} 
\end{array}
\right ) &
= &
\begin{pmatrix}
{\color{red}a_2b_3}{\color{green!50!black}-a_3b_2}\\
{\color{white}a_3b_1}{\color{white}-a_1b_3}\\
{\color{white}a_1b_2}{\color{white}-a_2b_1}
\end{pmatrix}
\\
{\color{gray}a_1} & & {\color{gray}b_1} & & \\
\end{array}
\]}
%
\only<3>{\[
\begin{array}{ccccc}
\left (
\begin{array}{c}
{\color{gray}a_1} \\
{\color{gray}a_2} \\
{\color{red}a_3 }
\end{array}
\right ) &
\times &
\left (
\begin{array}{c}
{\color{gray}b_1} \\
{\color{gray}b_2 } \\
{\color{green!50!black}b_3} 
\end{array}
\right ) &
= &
\begin{pmatrix}
{\color{red}a_2b_3}{\color{green!50!black}-a_3b_2}\\
{\color{red}a_3b_1}{\color{green!50!black}-a_1b_3}\\
{\color{white}a_1b_2}{\color{white}-a_2b_1}
\end{pmatrix}
\\
{\color{green!50!black}a_1} & & {\color{red}b_1} & & \\
\end{array}
\]}
%
\only<4>{\[
\begin{array}{ccccc}
\left (
\begin{array}{c}
{\color{red}a_1} \\
{\color{green!50!black}a_2} \\
{\color{gray}a_3 }
\end{array}
\right ) &
\times &
\left (
\begin{array}{c}
{\color{green!50!black}b_1} \\
{\color{red}b_2 } \\
{\color{gray}b_3} 
\end{array}
\right ) &
= &
\begin{pmatrix}
{\color{red}a_2b_3}{\color{green!50!black}-a_3b_2}\\
{\color{red}a_3b_1}{\color{green!50!black}-a_1b_3}\\
{\color{red}a_1b_2}{\color{green!50!black}-a_2b_1}
\end{pmatrix}
\\
{\color{gray}a_1} & & {\color{gray}b_1} & & \\
\end{array}
\]}

\end{frame}

\section{Erweiterungen}

\begin{frame}
\frametitle{Erweiterungen} Bis jetzt wurden Vektoren sowie das Rechnen
mit diesen im zwei- und dreidimensionalen Raum besprochen. Die
Komponenten dieser Vektoren waren reelle Zahlen.

\visible<2->{
Es sollte nicht
allzusehr \"uberraschen, da{\ss} dieser klassische und anschauliche
Vektorbegriff in mehrfacher Hinsicht erweitert werden kann.  
\begin{itemize}
\item Vier und mehr Dimensionen, ja sogar unendlich viele Dimensionen
\item Komponenten des Vektors sind komplexe Zahlen
\item Komponenten des Vektors sind Funktionen (vektorw\"artige Funktionen)
\item Differential- und Integralrechnung vektorw\"artiger Funktionen (Vektoranalysis)
\end{itemize}
Die folgenden Folien sollen diese Punkte illustrieren.}
\end{frame}

\begin{frame}
\frametitle{Vektoren in vier und mehr Dimensionen}
In manchen Anwendungen ergibt sich die Notwendigkeit vier (oder auch viel mehr)
Dimensionen ``zusammenzufassen'' --- Vektoren bieten sich geradezu an. Beispiele:
\begin{itemize}
\item Relativit\"atstheorie: Die Zeit $t$ wird als vierte Dimension hinzugenommen. Genaugenomment ist die vierte Komponente komplex, ein Vektor im Minkowskiraum
hat die Form $\vec x=(x,y,z,ict)$, $c$ ist Lichtgeschwindigkeit.
\item<2-> Koordinaten, Geschwindigkeiten etc.\ von Vielteilchensystemen:
Denken Sie an ein Protein mit seinen hunderten oder tausenden
Atomen. Jedes dieser insgesamt $N$ Atome hat Koordinaten $\vec
r_i=(x_i,y_i,z_i)=(r_{i_x},r_{i_y},r_{i_z})$, mit $i=1,\ldots,N$. Es ist oft zweckm\"a{\ss}ig,
diese $N$ Koordinaten zu einer einzigen ``Ortskoordinate'' $\vec R$
zusammenzufassen, was einem Vektor der Dimensionalit\"at $3N$
entspricht:
\[\vec R=(r_{1_x},r_{1_y},r_{1_z},r_{2_x},r_{2_y},r_{2_z},\ldots,r_{N_x},r_{N_y},r_{N_z})\]
Analog kann man z.B. auch die $N$ Geschwindigkeiten $\vec v_i$ ($i=1,\ldots,N$) aller Atome (Atome bewegen sich!) zu 
\[\vec V=(v_{1_x},v_{1_y},v_{1_z},v_{2_x},v_{2_y},v_{2_z},\ldots,v_{N_x},v_{N_y},v_{N_z})\]
zusammenfassen.
\item<3-> Der thermodynamische Zustand eines Mehrkomponentensystems kann z.B. durch$V$ (Volumen), $T$ (Temperatur) und $n_i$, $i=1,\ldots,k$, (Molmengen der
$k$ Komponenten) beschrieben werden. Je nach Anwendung kann es sinnvoll sein,
diese Gr\"o{\ss}en einzeln zu behandeln, oder zu einem Vektor $(V,T,n_1,n_2,\ldots,n_k)$ zusammenzufassen.
\end{itemize}
\visible<4->{Manchmal dient ein derartiges Zusammenfassen blo{\ss} der kompakten Notation, sehr oft wird mit diesen h\"oherdimensionalen Vektoren auch gerechnet.}
\end{frame}

\begin{frame}
\frametitle{Rechnen mit h\"oherdimensionalen Vektoren}
\begin{itemize}
\item Alle besprochenen Rechenregeln gelten wie in 2D und 3D, mit folgender
\item \alert{Ausnahme:} Das Vektorprodukt ist nur f\"ur dreidimensionale Vektoren definiert und sinnvoll!
\end{itemize}
Beispiele: Sei
\[\vec a=\begin{pmatrix} 1 \\
                    2 \\
                    3 \\
                    4
\end{pmatrix},\quad \vec b=\begin{pmatrix} -1 \\
                    3 \\
                    -4 \\
                    2
\end{pmatrix}\]
Dann ist:
\visible<2->{\[ \vec a +\vec b=
\begin{pmatrix} 1 + (-1)\\
                    2 + 3 \\
                    3 + (-4)\\
                    4 +2
\end{pmatrix} =
\begin{pmatrix} 0 \\
                    5 \\
                    -1 \\
                    6
\end{pmatrix}
\]
\visible<3->{\[
\vec a\cdot\vec b=1\cdot(-1)+2\cdot3+3\cdot(-4)+4\cdot2=+1
\]
\visible<4->{\[ \vec a \times \vec b = \text{\em nicht definiert!} \]

\visible<5->{\small Generisch definieren sich h\"oherdimensionale Vektoren
(bzw. die Vektorr\"aume zu denen sie geh\"oren) dadurch, da{\ss} ein
Mindestma{\ss} von Operationen in ihnen gestattet/definiert ist
(z.B. Abgeschlossenheit bez\"uglich (einer m\"oglicherweise verallgemeinerten) Addition, Kommutativit\"at und
Assoziativit\"at dieser Addition, Existenz des Nullelements und des
inversen Elements, Abgeschlossenheit bez\"uglich skalarer
Multiplikation, dar\"uberhinaus oft Existenz der Norm
(``verallgemeinerte L\"ange'') und eines Skalarprodukts)}}}}

\end{frame}

\begin{frame}
\frametitle{Vektoren mit komplexen Komponenten}
Wir betrachen die zwei Vektoren
\[\vec a=\begin{pmatrix} 1 \\
                    2+i \\
                    -3i 
\end{pmatrix},\quad \vec b=\begin{pmatrix} -i \\
                    3 \\
                    -4-i 
\end{pmatrix}\]
und wollen $\vec a+\vec b$, sowie $\vec a\cdot \vec b$ berechnen.
F\"ur die Addition \"andert sich nichts, es gilt
\[\vec a+\vec b=\begin{pmatrix} 1 + (-i) \\
                    2+i + 3\\
                    -3i + (-4 -i)
\end{pmatrix}
=
\begin{pmatrix}
1-i \\
5+i \\
-4-4i
\end{pmatrix}
\]
in v\"olliger Analogie zur Addition reeller Vektoren.\\[.5cm]

\end{frame}

\begin{frame}{Vektoren mit komplexen Komponenten}
Etwas anders sieht es f\"ur das Skalarprodukt aus. Dieses wird definiert
als
\[\vec a\cdot \vec b=a_x\cdot \alert{b_x^*}+a_y\cdot \alert{b_y^*}+a_z\cdot \alert{b_z^*},\]
wobei der Stern die konjugiert komplexe Zahl bedeutet. (Analoge Definitionen 
gelten f\"ur 2D und h\"oherdimensionale komplexe Vektoren!) F\"ur unser Beispiel
hei{\ss}t das:
\[\vec a=\begin{pmatrix} 1 \\
                    2+i \\
                    -3i 
\end{pmatrix},\quad \vec b=\begin{pmatrix} -i \\
                    3 \\
                    -4-i 
\end{pmatrix}\]
\[\vec a\cdot \vec b=
1\cdot(\alert{+}i)+(2+i)\cdot3+(-3i)\cdot(-4\alert{+}i)=\]
\[i+6+3i+12i\underbrace{-3i^2}_{+3}=9+15i
\]


\visible<2->{\vspace*{3ex}Die Definition des komplexen Skalarprodukts erkl\"art sich wie
folgt: Es sollte nach wie vor $\left|\vec
a\right|=\sqrt{\vec a\cdot\vec a}$ gelten, und das Ergebnis dieser Operation
sollte einer L\"ange (= positive, reelle Zahl!) entsprechen. W\"urden wir $\vec
a\cdot\vec a=a_x^2+a_y^2+a_z^2$ berechen, so erhielten wir (mit
unseren Zahlen) $\left|\vec
a\right|=\sqrt{-5+4i}$, also eine komplexe Zahl. Verwenden wir hingegen die
korrekte Definition $\vec
a\cdot\vec a=a_x a_x^*+a_y a_y^*+a_z a_z^*$, so ist $\left|\vec
a\right|=\sqrt{1\cdot 1+(2+i)\cdot(2\alert{-}i)+(-3i)(\alert{+}3i)}=+\sqrt{15}\in\mathbb{R}$}
\end{frame}

\begin{frame}
\frametitle{Funktionen als Vektorkomponenten}
Es sei $\vec r$ die Ortskoordinate eines sich bewegenden Massenpunkts im
Raum. In diesem Fall werden sich die Komponenten des Vektors als Funktion
der Zeit \"andern, d.h.,
\[\vec r=
\begin{pmatrix}
x(t) \\
y(t) \\
z(t) 
\end{pmatrix}
=\vec r(t)
\]
\visible<2->{Die Geschwindigkeit dieses Massenpunkts ergibt sich durch
Differentiation nach der Zeit $t$. Man erh\"alt die Ableitung $d\vec
r(t)/dt=\Dot{\Vec r}(t)$ indem man die Komponenten des Vektors
differenziert, d.h.
\[\frac{d\vec r(t)}{dt}=\Dot{\Vec r}(t)=\begin{pmatrix}
\dot x(t) \\
\dot y(t) \\
\dot z(t) 
\end{pmatrix}
\]
\visible<3->{Ganz analog l\"a{\ss}t sich auch ein Vektor, dessen Komponenten Funktionen
einer Variablen sind, integrieren, d.h.
\[\int\vec a(u)du=\begin{pmatrix}
\int a_x(u)du \\
\int a_y(u)du \\
\int a_z(u)du 
\end{pmatrix}
\]}}
\end{frame}

\begin{frame}
\frametitle{Der Gradient / Nablaoperator}
Zu einer Funktion von $n$ Ver\"anderlichen $f(x_1,x_2,\ldots,x_n)$ kann man
$n$ partielle Ableitungen $\partial f/\partial x_1$, \ldots,  $\partial f/\partial x_n$ bilden. Fa{\ss}t man diese $n$ Ableitungen zu einem Vektor zusammen, so
bezeichnet man diesen als Gradient von $f$ (grad~f, Symbol $\vec\nabla f$):
\[\vec\nabla f=\left(\frac{\partial f}{\partial x_1},\ldots,\frac{\partial f}{\partial x_n}\right)\]
Sucht man z.B. Extremstellen von $f$, so ist die notwendige Voraussetzung f\"ur Kanditatenpunkte, da{\ss} die ersten, partiellen Ableitungen verschwinden. Mit Hilfe des Gradienten, kann man diese Bedingung kompakt als
\[\vec\nabla f=0\]
schreiben.
\end{frame}

\begin{frame}{Der Gradient / Nablaoperator}

Das Symbol $\vec\nabla$ wird als {\em Nablaoperator} bezeichnet:
\[\vec \nabla=\left(\frac{\partial }{\partial x_1},\frac{\partial }{\partial x_2},\ldots,\frac{\partial }{\partial x_n}\right)=
\frac{\partial }{\partial x_1} \vec e_1+\frac{\partial }{\partial x_2}\vec e_2+\ldots+\frac{\partial }{\partial x_n}\vec e_n
\]
L\"a{\ss}t man diesen auf eine Funktion $f$ operieren, so erh\"alt man wie oben gezeigt den Gradienten 
$\vec\nabla f$.


\visible<2->{\vspace*{2ex}Der Nablaoperator macht aus der {\em skalaren} Funktion
$f$ ($f$ weist einem $n$-Tupel von Werten $(x_1,\ldots,x_n)$ in eindeutiger
  Weise eine Zahl(=Skalar!) zu) einen Vektor.


  \visible<3->{\vspace*{2ex}Im allgemeinen ist jede
Komponente des Gradienten wieder eine Funktion aller $n$ Variablen $x_1,\ldots,x_n$. Ein physikalisches Beispiel eines Gradienten ist die Kraft $\vec F$,
\[\vec F=-\vec\nabla V\]
wobei $V$ die potentielle Energie des Systems ist.}}
\end{frame}

\begin{frame}
\frametitle{Gradient, Potentielle Energie, Kraft --- ein Beispiel}
Wir betrachten die Coulombwechselwirkung zwischen zwei Ladungen $q_1$ und
$q_2$ mit Koordinaten $\vec r_1=(x_1,y_1,z_1)$, $\vec r_2=(x_2,y_2,z_2)$.
Die potentielle Energie $V_{elec}$ als Funktion des Abstands $r=\left|\vec r_2-\vec r_1\right|$ ist 
\[V_{elec}\propto\frac{q_1 q_2}{r}\] 
{\small Das Proportionalit\"atszeichen $\propto$ deutet an, da{\ss} je nach Einheitensystem noch ein Vorfaktor fehlt. $V_{elec}=V_{elec}(x_1,y_1,z_1,x_2,y_2,z_2)$, da $r=\sqrt{(x_2-x_1)^2+(y_2-y_1)^2+(z_2-z_1)^2}$.}

Wir suchen z.B. die Kraft $\vec F_1$, die auf Teilchen 1 wirkt, d.h.,
\[\vec F_1=-\vec\nabla_1 V_{elec}=
-\left(\frac{\partial }{\partial x_1},\frac{\partial }{\partial y_1},\frac{\partial }{\partial z_1}\right)V_{elec}=-\left(\frac{\partial V_{elec}}{\partial x_1},\frac{\partial V_{elec}}{\partial y_1},\frac{\partial V_{elec}}{\partial z_1}\right)
\]
{\small(der Subskript 1 in $\vec\nabla_1$ verdeutlicht, da{\ss} uns nur die Ableitungskomponenten von Teilchen (Ladung) 1 interessieren)}

Eine kurze Rechnung ergibt z.B. (bitte erg\"anzen Sie die fehlenden Schritte!)
\[\frac{\partial }{\partial x_1}\frac{q_1 q_2}{r}=\ldots
=+\frac{q_1\, q_2\,(x_2-x_1)}{[(x_2-x_1)^2+(y_2-y_1)^2+(z_2-z_1)^2]^{3/2}}=
+\frac{q_1\, q_2\,(x_2-x_1)}{r^3}
\]
Somit ist 
\[
\vec F_1=-\vec\nabla_1 V_{elec}=-\frac{q_1q_2}{r^3}
\begin{pmatrix}
x_2-x_1\\
y_2-y_1\\
z_2-z_1
\end{pmatrix}=
-\frac{q_1q_2}{r^3}\vec r
\]
(Auch ohne Mathematik sollte klar sein, da{\ss} $\vec F_2=-\vec F_1$ ist.)

\end{frame}

\input{gpl}

\end{document}


