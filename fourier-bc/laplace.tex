\documentclass[t,xcolor=dvipsnames,8pt]{beamer}

\usepackage{calc}
\usepackage{tikz}
\usepackage{pgfplots}
\usepackage{cancel}
\usepackage{spot}
% This file is a solution template for:

% - Talk at a conference/colloquium.
% - Talk length is about 20min.
% - Style is ornate.



% Copyright 2004 by Till Tantau <tantau@users.sourceforge.net>.
%
% In principle, this file can be redistributed and/or modified under
% the terms of the GNU Public License, version 2.
%
% However, this file is supposed to be a template to be modified
% for your own needs. For this reason, if you use this file as a
% template and not specifically distribute it as part of a another
% package/program, I grant the extra permission to freely copy and
% modify this file as you see fit and even to delete this copyright
% notice. 


\mode<presentation>
{
  \usetheme{default}
%  \setbeamercovered{transparent}
  \setbeamertemplate{navigation symbols}{}
}


\usepackage[english]{babel}
% or whatever

%\usepackage[latin1]{inputenc}
% or whatever

%\usepackage{times}
%\usepackage[T1]{fontenc}
\usepackage{type1cm}
% Or whatever. Note that the encoding and the font should match. If T1
% does not look nice, try deleting the line with the fontenc.

\usepackage{amsmath}
%\usepackage{pdfanim}
%\usepackage{multimedia}
%\usepackage{movie15}
\usepackage{alltt}
\usepackage{spot}

\newcommand{\numb}[1]{\multicolumn{1}{r}{$#1\;\;\;\;$}}
\newcommand{\floor}[1]{\ensuremath{\left\lfloor #1 \right\rfloor}}

\title{Laplace Transformation}
%\subtitle{Legendresche DGL, Legendre Polynome}

%\subtitle
%{Include Only If Paper Has a Subtitle}

\author{Stefan Boresch}
% - Give the names in the same order as the appear in the paper.
% - Use the \inst{?} command only if the authors have different
%   affiliation.

\institute{
  Department of Computational Biological Chemistry\\
  Faculty of Chemistry\\
  University of Vienna
}

\date{21. Juni 2021}
%\date{December 7, 2006}
%\date{July 11/12, 2008}

\subject{Mathematik f\"ur Biologische Chemie: Laplace Transformation}
% This is only inserted into the PDF information catalog. Can be left
% out. 


% If you have a file called "university-logo-filename.xxx", where xxx
% is a graphic format that can be processed by latex or pdflatex,
% resp., then you can add a logo as follows:

% \pgfdeclareimage[height=0.5cm]{university-logo}{university-logo-filename}
% \logo{\pgfuseimage{university-logo}}

% If you wish to uncover everything in a step-wise fashion, uncomment
% the following command: 

%\beamerdefaultoverlayspecification{<+->}


\begin{document}

\begin{frame}
  \titlepage
\end{frame}

\begin{frame}
%\frametitle{Lizenz}
\vspace*{1cm}

\begin{center}
Copyright (c) 2009,2020,2021 Stefan Boresch
\end{center}

\vspace*{1cm} Permission is granted to copy, distribute and/or modify
this document under the terms of the
\href{http://www.gnu.org/licenses/fdl.html}{\color{blue}GNU Free Documentation
License, Version 1.2} or any later version published by the Free
Software Foundation; with no Invariant Sections, no Front-Cover Texts,
and no Back-Cover Texts.  A copy of the license is included in the
section entitled "GNU Free Documentation License".
\end{frame}


\begin{frame}{Rekapitulation Fourierentwicklung/-transformation}

  \[g(k)=\hat f(k)=F[f(x)]=\frac{1}{\sqrt{2\pi}}\int_{-\infty}^\infty f(x)e^{-ikx}\,dx\]
  \[f(x)=F^{-1}[\hat f(k)] =F^{-1}[g(k)] =\frac{1}{\sqrt{2\pi}}\int_{-\infty}^\infty g(k)e^{+ikx}\,dk\]

  \begin{block}<2->{Anmerkungen}
    \begin{itemize}
    \item Transformation zwischen $x$- und $k$-Raum, diese entsprechen oft physikalisch wichtigen 'Paaren', z.B. Zeit und Frequenz
    \item Manche Zusammenh\"ange werden im Fourierraum / $k$-Raum ``einfacher'', z.B.
      \begin{itemize}
      \item DGLen
      \item Faltungstheorem
        \end{itemize}
    \end{itemize}
  \end{block}

  \begin{block}<3->{Motivation}
    \begin{itemize}
    \item Nicht jede Funktion $f(x)$ kann Fourier transformiert werden
    \item Die ``Idee'' DGLen in algebraische Gleichungen umzuformen ist jedenfalls verlockend
      \item Gibt es andere ``Fourier-artige'' n\"utzliche Transformationen?
      \end{itemize}
    \end{block}

\end{frame}

\begin{frame}{Was versteht man unter der Transformation einer Funktion?}

  \begin{block}{Ein paar bekannt (``allt\"agliche'') Transformationen}
    \begin{itemize}
      \item $\displaystyle{D[f(x)]=f'(x)}$ (Differentiation)
      \item $\displaystyle{I[f(x)]=\int_0^x f(t)dt}$ (Integration)
      \item $\displaystyle{M_g[f(x)]=g(x)f(x)}$ (Multiplikation mit $g(x)$)
    \end{itemize}
    \end{block}

  \begin{block}<2->{Lineare Transformationen}
    \[T[\alpha f(x)+\beta g(x)]=\alpha T[f(x)]+\beta T[g(x)]\]
    \end{block}

  \begin{block}<3->{Verallgemeinerung der Integraltransformation}
    \begin{itemize}
    \item Die Fouriertransformation ist etwas komplexer als $I[f(x)]$, gen\"ugt aber
      dem folgenden allgemeineren Schema einer
    \item Integraltransformation:
      \[T[f(x)]=\int_a^bK(s,x)f(x)dx=\hat f(s)\]
      $K(s,x)$ wird als ``Kern'' (kernel) bezeichnet
      \item F\"ur die Fouriertransformation gilt: $a=-\infty$, $b=+\infty$, $K(s,x)=\displaystyle{1/\sqrt{2\pi}\,e^{-isx}}$ (wobei wir bis jetzt immer $k$ statt $s$ geschrieben haben). Derartige Integraltransformationen (und somit auch die FT) sind linear
      \end{itemize}
    \end{block}

\end{frame}

\begin{frame}{Eine weitere Integraltransformation --- Laplace Transformation}

  \begin{itemize}
  \item Damit die FT einer Funktion existiert, muss $f(x)$ jedenfalls st\"uckweise stetig sein, und es muss die Bedingung erf\"ullt sein, dass $\displaystyle{\int_{-\infty}^\infty|f(x)|dx}$  endlich ist (wobei letztere Bedingung in einigen/vielen interessanten F\"allen umgangen werden kann!).
  \item<2-> Der Faktor $e^{\pm ikx}$ macht bei der Integration typischerweise nicht allzu viele Problem.
  \item<3-> Viele Probleme sind einseitig ($x\ge 0$), z.B. nur der Fall positiver Zeit ($t\ge 0$) ist von Interesse.
  \item<4-> Es sei $f(x)$ wieder st\"uckweise stetig und derma{\ss}en beschr\"ankt, dass
    \[|f(x)|< Ke^{\mathrm{Re}[s]x}\] f\"ur alle $x>x_0$ ($K\in\mathbb{R}, s\in\mathbb{C}$). Dann kann man
    aus der FT eine modifizierte Integraltransformation, die \spot<4>{Laplace} Transformation, ableiten:
    \[L[f(x)]=\hat f(s)=\int_0^\infty f(x)e^{-sx}dx\]
  \item<5-> Die Formel f\"ur die \spot<5>{inverse} Laplacetransformation $L^{-1}[]$ ben\"otigt (1) die exakte Herleitung, und (2) die direkte Berechnung des
    auftretenden Integrals ben\"otigt i.a.\ Kontourintegration.
    \item<6-> Laplace Transformationspaare sind extensiv tabelliert bzw. CAS (z.B. Mathematica (\texttt{LaplaceTransform[f,x,s]}, \texttt{InverseLaplaceTransform[g,s,k]})) zu Hilfe!
    \end{itemize}
  \end{frame}

\begin{frame}{Laplace Transformation}
  Auch ohne invertieren zu k\"onnen, ist die Laplace Transformation n\"utzlich, was jetzt an ein paar Beispielen illustriert werden soll.

  \visible<2->{Zun\"achst einmal eine kleine  Tabelle $\hat f(s)=L[f(x)]$

  \begin{center}
    \begin{tabular}{ll}
      $f(x)$ & $\hat f(s)$\\[2ex]
      $1$ & $\displaystyle{\frac{1}{s}}$\\[2ex]
      $x$ & $\displaystyle{\frac{1}{s^2}}$\\[2ex]
      $x^n$ & $\displaystyle{\frac{n!}{s^{n+1}}}$\\[2ex]
      $e^{ax}$ & $\displaystyle{\frac{1}{s-a}}$ (mit $s>a$!)\\[3ex]
      $\sin ax$& $\displaystyle{\frac{a}{s^2+a^2}}$\\[3ex]
      $\cos ax$ & $\displaystyle{\frac{s}{s^2+a^2}}$\\[3ex]
    \end{tabular}
  \end{center}

  Bitte rechnen Sie die einfachsten Transformationen per Hand nach und \"uberpr\"ufen Sie die restlichen Eintr\"age mit Mathematica! \visible<3->{
  Die Laplace Transformation ist klarerweise linear, wir k\"onnen z.B. sofort
  \[L[2x+3]=2L[x]+3L[1]=2/s^2+3/s\]
  ablesen.}}
  
  
  \end{frame}

\begin{frame}{Anwendung auf DGLs}
  \[y''+py'+qy=h(x)\visible<2->{\Rightarrow L[y''+py'+qy]=L[h(x)]
  \visible<3->{\Rightarrow L[y'']+pL[y']+qL[y]=\hat h(s)}}
  \]
  \visible<4->{Was ist $L[y'(x)]$?:
    \[L[y'(x)]=\int_0^\infty e^{-sx}y'(x)\,dx=y(x)e^{-sx}\Big|_0^\infty+s\int_0^\infty y(x) e^{-sx}dx=
    -y(0)+sL[y(x)]\]
  }
  \visible<5->{Es gilt also
    \[L[y']=sL[y]-y(0)\]
    und (bitte selbst nachrechnen!)
    \[L[y'']=s^2L[y]-sy(0)-y'(0)\]
  }
  \visible<6->{Somit bekommen wir f\"ur die obige DGL
    \[\underbrace{s^2L[y]-sy(0)-y'(0)}_{L[y'']}+p\underbrace{(sL[y]-y(0))}_{L[y']}+qL[y]=\hat h(s)\]
    bzw.\
    \[L[y]=\frac{\hat h(s)+(s+p)y(0)+y'(0)}{s^2+ps+q}\]
  }%
  \visible<7->{Wie auch bei der FT angemerkt, ist dies effizient, wenn man die Inverse ``einfach'' berechnen kann. Insbesondere funktioniert das auch bei {\em unstetigen} $h(x)$!}
  \end{frame}

\begin{frame}{Beispiel:}

  \[y''+4y=4x\qquad y(0)=1\quad y'(0)=5\]
  [per Hand!]
  \end{frame}


\begin{frame}{Eigenschaften der Laplace Transformation (LT)}
  \[L[y'(x)]=sL[y(x)]-y(0)\]
    Gilt auch eine Umkehrung, d.h., was ist
    \[L\left[\int_0^xy(\xi)d\xi\right]=?\]
\visible<2->{    Mit
    \[\int_a^b u\,dv=u\,v\Big|_a^b-\int_a^bv\,du\]
    \[du=y(\xi)\Rightarrow u=\int_0^xy(\xi)d\xi\qquad v=-e^{-sx}/s\Rightarrow dv=e^{-sx}dx\]}
\visible<3->{    Damit
    \[\alert<4>{L\left[\int_0^xy(\xi)d\xi\right]}=\int_0^\infty\!\!\left(\int_0^x\!\!y(\xi)d\xi\right)e^{-sx}dx=
    \left[-\frac{e^{-sx}}{s}\!\!\int_0^xy(\xi)d\xi\right]_0^\infty-
    \int_0^\infty\!\!
    \left(-\frac{e^{-sx}}{s}\right)y(x)dx\]
    \[=\underbrace{\left[-\frac{e^{-sx}}{s}\!\!\int_0^xy(\xi)d\xi\right]_0^\infty}_{=0}
    +\frac{1}{s}\underbrace{\int_0^\infty y(x)e^{-sx}dx}_{L[y(x)]}=\alert<4>{\frac{\hat y(s)}{s}}\]
    (NB: F\"ur $x\rightarrow\infty$ verlassen wir uns, dass der Vorfaktor $e^{-sx}$ gen\"ugend d\"ampfend ist (Voraussetzung f\"ur die LT!), bei $x=0$ werden obere und untere
    Grenze ident und somit verschwindet das Integral!)}

\end{frame}


\begin{frame}{Eigenschaften der Laplace Transformation (LT)}

  \begin{itemize}
  \item Mittels Partialbruchzerlegung kann oft eine LT in einfachere Terme zerlegt werden, die dann mit einer Tabelle r\"ucktransformiert werden k\"onnen.
  \item<2-> Weiters
    \[L[e^{ax}y(x)]=\int_0^\infty y(x)e^{ax}e^{-sx}=\int_0^{\infty} y(x)e^{-(s-a)x}=\hat y(s-a)\]
    NB: $s$ kann immer so gew\"ahlt werden ($s>a$), dass das Integral konvergiert!
  \item<3-> Beispiel:
    \[L^{-1}[1/s^2]=x\quad\Rightarrow\quad L^{-1}[1/(s-3)^2]=e^{3x}x\]
  \item<4-> Mit $\hat y(s)=\int_0^\infty y(x)e^{-sx}dx$ betrachten wir jetzt
    \[\frac{d\hat y(s)}{\alert<4>{ds}}=\hat y\alert<4>{'}(s)=\int_0^\infty e^{-sx}(-x)y(x)dx\]
    \[\Rightarrow\qquad L[-xy(x)]=\hat y'(s)\]
  \item<5-> Differenziert man weiter, findet man leicht die Verallgemeinerung
    \[L[(-1)^n x^ny(x)]=\hat y^{(n)}(s)\]
    \end{itemize}
  \end{frame}

\begin{frame}{Eigenschaften der LT}
  \begin{itemize}
  \item In Fortsetzung der vorigen Seite gilt auch
    \[L[xy]=-\frac{d}{ds}L[y]=-\hat y'(s)\]
  \item<2-> Und nochmals:
    \[L[x y']=-\frac{d}{ds}L[y']=-\frac{d}{ds}[s\hat y(s)-y(0)]=-\frac{d}{ds}[s\hat y(s)]\]
  \item<3-> Bitte selbst nachrechnen, dass
    \[L[xy'']=\ldots=-\frac{d}{ds}[s^2\hat y(s)-sy(0)]\]
  \item<4-> Zum Schluss ein Integralzusammenhang: Es gilt
    \[L\left[\frac{y(x)}{x}\right]=\int_s^\infty\hat y(s)ds\qquad (*)\]
    \visible<5->{Es sei $L[y(x)/x]=G(s)$. Dann ist doch $dG(s)/ds=L\left[-x\displaystyle{\frac{y(x)}{x}}\right]=-L[y(x)]=$ $-\hat y(s)$. Wenn man die Differentiation nach $s$ wieder 'gut' macht, gilt also weiters $G(s)=-\displaystyle{\int_a^s\hat y(s')ds'}$. Hierbei ist $a$ eine arbitr\"are fixe Grenze. Durch Vertauschen der Grenzen werden wir das $-$ los, und wegen $G(s)\rightarrow 0$ f\"ur $s\rightarrow \infty$ setzen wir $a\rightarrow \infty$. Daraus folgt (*)}
    \end{itemize}
  \end{frame}

\begin{frame}{Anwendung der Integraleigenschaft}
  Schreiben wir das Ganze nochmals
  \[L\left[\frac{y(x)}{x}\right]=\int_0^\infty e^{-sx}\frac{y(x)}{x}dx=\int_s^\infty\hat y(s)ds\]
  F\"ur $s\rightarrow 0$ gibt das
  \[\int_0^\infty\frac{y(x)}{x}dx=\int_0^\infty\hat y(s)ds\]
\visible<2->{\vspace*{2ex}{\bfseries Anwendung:} $L[\sin x]=1/(s^2+1)$ Somit gilt aber
  \[\int_0^\infty\frac{\sin x}{x}dx=\int_0^\infty\frac{1}{s^2+1}ds=\arctan s\Big|_0^\infty=\frac{\pi}{2}\]
  was eine Variante der im 'Appendix' der letzen VO gezeigten Herleitung darstellt.}
  \end{frame}


\begin{frame}{Noch ein paar Laplace Transformationen}

  \begin{itemize}
    \item Heaviside Funktion $\Theta(x)\Rightarrow \hat{\Theta}(s)=\displaystyle{\frac{1}{s}}$ (Haus\"ubung!)
    \item<2-> $L[\delta(x)]$ Ganz offensichtlich gilt einmal
      \[L[\delta(x-a)]=\int_0^\infty\delta(x-a)e^{-sx}dx=e^{-as}\quad (a>0)\]
        \visible<3->{Aus dem Grenzwert $a\rightarrow 0$ [NB: keine mathematisch legitime Herleitung!] folgt
        \[\lim_{a\rightarrow 0}L[\delta(x-a)]=L[\delta(x)]=\lim_{a\rightarrow 0}e^{-as}=1\]}
      \item<4-> Verbindung zur $\Gamma$-Funktion
        \[\Gamma(z)=\int_0^\infty t^{z-1}e^{-t} dt\]
        \[\Gamma(n+1)=n!\qquad n\in\mathbb{N}\]
        \[L[x^n]=\frac{n!}{s^{n+1}}=\frac{\Gamma(n+1)}{s^{n+1}}\]
        \visible<5->{damit ergibt sich aber folgende Verallgemeinerung
        \[L(x^{\alert{z}})=\frac{\Gamma(\alert{z+1})}{s^{\alert{z+1}}}\qquad (\mathrm{Re}[z]>-1,\mathrm{Re}[s]>0)\]}
    \end{itemize}
  \end{frame}

\begin{frame}{Faltung und LT}

  In Zusammenhang mit der FT sind wir kurz auf Faltungen eingegangen,
  \[\int_{-\infty}^{\infty}f(\xi)g(x-\xi)d\xi\]
\visible<2->{Derartige Integrale treten auch mit ``anderen Grenzen'' auf, eine wichtige Variante ist
  \[\int_{0}^{x}f(\xi)g(x-\xi)d\xi\]
  bzw.
  \[\int_{0}^{t}f(\tau)g(t-\tau)d\tau\qquad\text{(*)}\]}%
  \visible<3->{Eine physikalische Interpretation ist ein System zu einem Zeitpunkt $t$ (``jetzt''), das in
  der Vergangenheit $\tau$ einer Anregung ausgesetzt war. Die Auswirkung auf das System ist linear
  und h\"angt von der Gr\"o{\ss}e der Anregung ab, d.h., $f(\tau)d\tau$. Weiters h\"angt der Effekt
  auf die Gegenwart von der vertrichenen Zeit $t-\tau$ ab, $g(t-\tau)$ ist der Proportionalit\"atsfaktor. Dies motiviert den Integranden
  \[f(\tau)g(t-\tau)d\tau\]
  Auf Grund der (angenommenen) Linearit\"at addieren sich Anregungen, die zwischen 0 und $t$ passiert sind, dies f\"uhrt auf das Faltungsintegral (*). NB: Wenn Sie von Faltungen sprechen / mit Faltungen operieren, bitte aufpassen, ``welche Art'' genau gemeint ist!}
  
  \end{frame}

\begin{frame}{Faltung und LT}
  Wie f\"ur die FT gibt es auch f\"ur die LT ein Faltungstheorem
  \[L[f]\,L[g]=L[f\star g]=L\left[\int_0^t f(t-\tau)g(\tau)d\tau\right]\]
  \visible<2->{Hier eine Ableitung:
  \[L[f]\,L[g]=\hat f(s)\hat g(s)=\left[\int_0^\infty f(t')e^{-st'}dt'\right]
  \left[\int_0^\infty g(\tau)e^{-s\tau}d\tau\right]
  \]
  \[\int_0^\infty\int_0^\infty f(t')g(\tau)e^{-s(t'+\tau)}\,dt'\,d\tau
  \int_0^\infty\left[\int_0^\infty f(t')e^{-s(t'+\tau)}\,dt'\right]\,g(\tau)\,d\tau
  \]}
  \visible<3->{Die Integration erstreckt sich \"uber den ersten Quadranten ($t'\ge 0$, $\tau\ge 0$):
  
  \begin{center}
  \begin{tikzpicture}[domain=0:5,scale=0.75]
    \filldraw[cyan!30!white] rectangle (4.2,4.2);
    \draw[->] (-0.1,0) -- (4.2,0) node[right] {$t'$}; 
    \draw[->] (0,-0.1) -- (0,4.2) node[above] {$\tau$};
    \end{tikzpicture}  
    \end{center}}
\end{frame}

\begin{frame}{Faltung und LT}
  Wir setzen jetzt $t'+\tau=t$, es gilt $t'=t-\tau$ und $dt'=dt$. Aufpassen m\"ussen wir bei der (unteren) Grenze, $t'=0$ bedingt $t=\tau$. Dies f\"uhrt auf

  \[  \int_0^\infty\left[\int_0^\infty f(t')e^{-s(t'+\tau)}\,dt'\right]\,g(\tau)\,d\tau=
  \int_0^\infty\left[\int_\tau^\infty f(t-\tau)e^{-st}\,dt\right]\,g(\tau)\,d\tau=\]
  \[=  {\color{ForestGreen}\int_0^\infty}{\color{orange}\int_\tau^\infty} f(t-\tau)e^{-st}\,g(\tau)\,{\color{orange}dt}\,{\color{ForestGreen}d\tau}\]

  \visible<2->{\vspace*{2ex}Hier der neue Integrationsbereich
  \begin{center}
      \begin{tikzpicture}[domain=0:5,scale=0.75]
    \filldraw[cyan!30!white] (0,0) -- (4.2,0) -- (4.2,4.2) -- cycle;
    \draw[->] (-0.1,0) -- (4.2,0) node[right] {$t$}; 
    \draw[->] (0,-0.1) -- (0,4.2) node[above] {$\tau$};
    \draw[->] (0,0) -- (4.2,4.2) node[above] {$\tau=t$};
    \draw[very thin,dashed] (1,0) -- (1,1);
    \draw[very thin,dashed] (0,1) -- (1,1);
    \draw[ForestGreen,->] (4.5,0) -- (4.5,4.2);% node[above] {$t=x$};
    \draw[orange,->,dotted,very thick] (1,-0.2) -- (4.2,-0.2);
    
    \end{tikzpicture}

    \end{center}}
\end{frame}

\begin{frame}{Faltung und LT}

  Wir vertauschen die Reihenfolge der Integration (mit enstprechender Anpassung der Grenzen!!)
  \vspace*{2ex}
  \begin{center}
    \begin{tabular}{cp{0.5cm}c}

      $\displaystyle{{\color{ForestGreen}\int_0^\infty}{\color{orange}\int_\tau^\infty} f(t-\tau)e^{-st}\,g(\tau)\,{\color{orange}dt}\,{\color{ForestGreen}d\tau}}$ & &
      $\displaystyle{{\color{orange}\int_0^\infty}\overbrace{{\color{ForestGreen}\int_0^t} f(t-\tau)\,g(\tau)\,{\color{ForestGreen}d\tau}}^{f\star g}\,e^{-st}\,{\color{orange}dt}}$\\[2ex]
      \begin{tikzpicture}[domain=0:5,scale=0.75]
        \filldraw[cyan!30!white] (0,0) -- (4.2,0) -- (4.2,4.2) -- cycle;
        \draw[->] (-0.1,0) -- (4.2,0) node[right] {$t$}; 
        \draw[->] (0,-0.1) -- (0,4.2) node[above] {$\tau$};
        \draw[->] (0,0) -- (4.2,4.2) node[above] {$\tau=t$};
        \draw[very thin,dashed] (1,0) -- (1,1);
        \draw[very thin,dashed] (0,1) -- (1,1);
        \draw[ForestGreen,->] (4.5,0) -- (4.5,4.2);% node[above] {$t=x$};
        \draw[orange,->,dotted,very thick] (1,-0.2) -- (4.2,-0.2);
        
      \end{tikzpicture}
      & &
      \begin{tikzpicture}[domain=0:5,scale=0.75]
        \filldraw[cyan!30!white] (0,0) -- (4.2,0) -- (4.2,4.2) -- cycle;
        \draw[->] (-0.1,0) -- (4.2,0) node[right] {$t$}; 
        \draw[->] (0,-0.1) -- (0,4.2) node[above] {$\tau $};
        \draw[->] (0,0) -- (4.2,4.2) node[above] {$\tau=t$};
        \draw[very thin,dashed] (1,0) -- (1,1);
        \draw[very thin,dashed] (0,1) -- (1,1);
        \draw[orange,->] (0,-0.2) -- (4.2,-0.2);% node[above] {$t=x$};
        \draw[ForestGreen,->,dotted,very thick] (-0.2,0) -- (-0.2,1);
        
      \end{tikzpicture}
    \end{tabular}
  \end{center}

  \vspace*{2ex}
  Das Integral auf der rechten Seite ist Laplacetransformierte der Faltung
  \[=\int_0^\infty e^{-st}\left[f\star g\right]dt=L[f\star g]\]
\end{frame}

\begin{frame}{DGLen, LT und Faltungstheorem}
  \[y''+py'+qy=h(x)\]
  \[L[y]=\frac{\hat h(s)+(s+p)y(0)+y'(0)}{s^2+ps+q}\]
  Unter der Bedingung/Annahme, dass $y(0)=y'(0)=0$ vereinfacht sich das nochmals
  \[L[y]=\frac{\hat h(s)}{\mathcal{P}(s)}\visible<3->{\equiv L[y_{\alert{h}}]}\]
  mit $\mathcal{P}(s)=s^2+ps+q$, einem Polynom in $s$.

\visible<2->{Als erstes betrachten wir zwei spezielle $h(x)$, n\"amlich die $\delta$-Funktion und die Heavisidefunktion $\Theta(x)$, f\"ur die gilt

  \[\begin{array}{lcc}
  L[\delta(x)]=1 & \Rightarrow & L[y_{\alert<3>{\delta}}]=\displaystyle{\frac{1}{\mathcal{P}(s)}}\\[2ex]
  L[\Theta(x)]=\frac{1}{s} & \Rightarrow & L[y_{\alert<3>{\Theta}}]=\displaystyle{\frac{1}{s\,\mathcal{P}(s)}}\\
  \end{array}
  \]
Sie k\"onnen sich dabei die Heaviside Funktion als ``Einschalten'' und die $\delta$-Funktion als
eine punktuelle St\"orung/Anregung bei $x=0$ vorstellen.}
  \end{frame}

\begin{frame}{DGLen, LT und Faltungstheorem}
\[L[y_h]=\frac{\hat h(s)}{\mathcal{P}(s)} \quad \Rightarrow \quad
\frac{1}{s}L[y_h]=\frac{\hat h(s)}{s\,\mathcal{P}(s)}=L[y_\Theta]L[h]\]%
\visible<2->{Mit dem Faltungstheorem kann ich das aber auch als
\[\frac{1}{s}L[y_h]=L[y_\Theta\star h(x)]=L\left[\int_0^x y_\Theta(x-\xi)h(\xi)d\xi\right]\]
schreiben.}\visible<3->{ Unter Verwendung von $L[f'(x)]=sL[f(x)]-f(0)$ erhalten wir weiters
\[L[{\color{blue}y_h}]=s\,L\left[\int_0^x y_\Theta(x-\xi)h(\xi)d\xi\right]=L\left[{\color{blue}\frac{d}{dx}\int_0^x y_\Theta(x-\xi)h(\xi)d\xi}\right]\]
\[y_h=\frac{d}{dx}\int_0^x y_\Theta(x-\xi)h(\xi)d\xi\]}
\visible<4->{F\"ur derartige Ableitungen gilt im allgemeinsten Fall mit $F(t)=\int_{u(t)}^{v(t)}G(t,x)dx$
{\small
  \[\frac{d}{dt}F(t)=\int_{u(t)}^{v(t)}\frac{\partial}{\partial t}G(t,x)dx+G(t,v)\frac{dv}{dt}-G(t,v)\frac{du}{dt}\]
}}
\visible<5->{Angewandt auf unseren Fall
\[y_h=\int_0^x y'_\Theta(x-\xi)h(\xi)d\xi+\overbrace{y_\Theta(x-x)}^{=0,\text{ Anfangsb.!}}h(x)\]}
\end{frame}

\begin{frame}{DGLen, LT und Faltungstheorem}
  Wegen $L[y_\Theta]L[h]=L[h]L[y_\Theta]$ gilt aber doch genauso
  \[\frac{1}{s}L[y_h(x)]=L[h(x)\star y_\Theta(x)]=L\left[\int_0^xh(x-\sigma)y_\Theta(\sigma)d\sigma\right]\Rightarrow \ldots\]
  \[y(x)=\int_0^xh'(x-\sigma)y_\Theta(\sigma)d\sigma+h(0)y_\Theta(x)\]
  Wir fassen die beiden Varianten zusammen und vereinfachen noch ein wenig
  \begin{eqnarray*}
    y(x) &=& \int_0^xy'_\Theta(x-\xi)h(\xi)d\xi+\overbrace{y_\Theta(0)}^{=0}h(x)\quad\text{wegen AB!}\\
    y(x) &=& \int_0^xh'(x-\sigma)y_\Theta(\sigma)d\sigma+h(0)y_\Theta(x)\\
         &=& \int_0^xy_\Theta(x-\xi)h'(\xi)d\xi+h(0)y_\Theta(x)\quad\text{nach }\xi=x-\sigma
  \end{eqnarray*}

  D.h., kennen wir die L\"osung $y_\Theta$ der DGL (f\"ur $h(x)=\Theta(x)$) bekommen wir die L\"osung
  f\"ur arbitr\"ares $h(x)$ durch Berechnen eines Faltungsintegrals.
\end{frame}

\begin{frame}{DGLen, LT und Faltungstheorem}
  Wenn wir die obigen Schritte mit $y_\delta$ statt $y_\Theta$ wiederholen, finden wir eine interessante Vereinfachung f\"ur $y'_\Theta$. Wir hatten gefunden
  \[L[y_{{\delta}}]=\displaystyle{\frac{1}{\mathcal{P}(s)}} \qquad L[y_{{\Theta}}]=\displaystyle{\frac{1}{s\,\mathcal{P}(s)}}\visible<2->{\quad\Rightarrow\quad  L[y_{{\Theta}}]=\frac{ L[y_{{\delta}}]}{s}}\]

  \visible<3->{Es gilt jetzt aber ganz allgemein, dass $L[\int_0^xf(x)dx]=L[f(x)]/s$ ist, somit
    ist $y_\delta=y'_\Theta$. Daraus folgt aber, dass das Faltungsintegral mit $y'_\Theta(x-\xi)$
    umgegschrieben werden kann, und wir finden eine weitere M\"oglichkeit, die allgemeine L\"osung
    der DGL auszudr\"ucken:
    \[y(x)=\int_0^x\alert{y_\delta}(x-\xi)h(\xi)d\xi\]
}
    
\end{frame}

\begin{frame}
  $\;$
  \end{frame}


%---------------------------------------------------------------------

\input{gpl}


\end{document}

