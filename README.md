Hier finden Sie den Latex Source Code meiner Unterlagen zu den Vorlesungen "[Mathematik fuer Molekulare Biologen](https://ufind.univie.ac.at/de/course.html?lv=301628&semester=2020W)" sowie "[Mathematik fuer Biologische Chemiker](https://ufind.univie.ac.at/de/course.html?lv=270058&semester=2021S)". Files / Unterlagen fuer letztere VO enden in -bc, d.h., das Verzeichnis 'vectors' gehoert zur Molekularen Biologie VO, das Verzeichnes 'vectors-bc' zur Biologischen Chemie.

In den Unterverzeichnissen finden Sie Files fuer Latex/Beamer, die Sie mit pdflatex uebersetzen muessen.
Zum vollstaendigen Bauen benoetigen Sie gnuplot, und das Ausfuehren von shell Commands waehrend des pdflatex Laufs muss erlaubt sein. Alternativ koennen Sie die beim ersten Durchlauf von pdflatex erzeugten \*\.gnuplot Files mit gnuplot verarbeiten (gnuplot \<file\>.gnuplot), damit werden die '\*\.table' Files die tikz zum Plotten braucht erzeugt.

NB: Die hier zur Verfuegung gestellten Files sind nur noetig, wenn Sie die PDF Dokumente selbst erstellen bzw. modifizieren wollen!

All Latex Sources are provided under the [GNU Free Documentation License 1.3](https://www.gnu.org/licenses/fdl-1.3.en.html) or later, which is reproduced verbatim in the File COPYING in this directory

Stefan Boresch, 2020, 2021
