\documentclass[12pt,compress]{beamer}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{lmodern}
\usepackage{tikz}
\usetikzlibrary{patterns}
\usepackage{tikz-cd}
%\tikzset{commutative diagrams/arrow style=math font}

% Beamer commands
\title{Introduction to Beamer and TikZ}
\author{Christian Schnell}
\date{April 7, 2017}
\setbeamertemplate{navigation symbols}{}
%\setbeamertemplate{blocks}[rounded][shadow=false]
%\usecolortheme{orchid}

% Code listing
\usepackage[listings]{tcolorbox}
\newtcblisting{source}{
	listing only,
	listing options={style=tcblatex,moredelim={**[is][\color{red}]{@}{@}}}
}


% TikZ options
\usetikzlibrary{arrows,shapes}
\tikzset{cloud/.pic={
\node[cloud, cloud puffs=15,cloud puff arc=110, aspect=2.6, draw, text width=3.1cm
    ] () at (0,0) {\tikzpictext};
}}

\begin{document}

\begin{frame}
\titlepage
\begin{tikzpicture}[overlay,remember picture]
\pic (c) at (9,1) [color=red,pic text=For people with bad memory\dots]{cloud};
\draw[->,color=red] (c) to[bend right] (7,3.65);
\pic (c) at (2.2,0) [color=red,pic text=In case you forgot today's date\dots]{cloud};
\draw[->,color=red] (c) to[bend left] (4.1,2.2);
\end{tikzpicture}
\end{frame}

% Beamer
\setbeamertemplate{footline}{}
\part{Computer presentations with Beamer}
\begin{frame}
\partpage
\end{frame}

\begin{frame}[fragile]{What is Beamer?}
Beamer is a \LaTeX-package for making computer presentations. Here is a
minimal example:
\begin{source}
\documentclass[12pt,compress]{@beamer@}

\title{The Title}
\author{The Author}
\date{The Date}

\begin{document}
\frame{\titlepage}
\end{document}
\end{source}
This produces a single slide with title, author, and date on it.
\end{frame}


% Beamer gets confused about \begin{frame} \end{frame}
\begin{tcboutputlisting}
\begin{frame}{My frame}
My text
\end{frame}

\begin{frame} 
\frametitle{My frame} 
My text
\end{frame}

\frame{
\frametitle{My frame}
My text
}
\end{tcboutputlisting}


\begin{frame}{Frames}
Slides are called ``frames'' in Beamer. 
\tcbinputlisting{listing only}
\end{frame}

% Same trick as above
\begin{tcboutputlisting}
@\part@{My part}

\begin{frame}
\partpage
\end{frame}
\end{tcboutputlisting}

\begin{frame}[fragile]{Parts}
If you like, you can organize your presentation into ``parts''. 
\tcbinputlisting{listing only,listing options={style=tcblatex,moredelim={**[is][\color{red}]{@}{@}}}}

The command \verb|\partpage| creates a title page for this part.

Parts are numbered automatically.
\end{frame}

\begin{frame}{Themes}
Beamer has many ``themes'', such as this one:
\begin{center}
\includegraphics[height=2.8in]{bad-example.png}
\end{center}
\begin{tikzpicture}[overlay,remember picture]
\uncover<2>{\node[align=center,black!20!white,font={\Huge\bfseries}] at (5.45,3.75)
{DO NOT USE THEMES!};}
\uncover<2>{\node[align=center,red,font={\Huge\bfseries}] at (5.4,3.8) 
{DO NOT USE THEMES!};}
\end{tikzpicture}
\end{frame}

\begin{frame}[fragile]{How to change the theme?}
The theme on the previous slide is called ``Warsaw''.
\begin{source}
\usetheme{Warsaw}
\usecolortheme{default}
\end{source}
Most themes waste a lot of space on each slide. Solution:
\begin{source}
\documentclass[12pt,compress]{beamer}
\setbeamertemplate{navigation symbols}{}
\end{source}
\pause
\begin{block}{Moral}
Use a plain white background instead of a fancy theme!
Highlight \alert{important} things using \verb|\alert{important}|.
\end{block}
\end{frame}

\begin{frame}{Theorems, Definitions, Corollaries, etc.}
Most themes will put a box around statements:
\begin{center}
\includegraphics[height=2.8in]{boxed-example.png}
\end{center}
\end{frame}

\begin{frame}[fragile]{How to state a theorem?}
On a white background, this effect can be achieved like this:
\begin{source}
\usecolortheme{orchid}
\begin{theorem}
Statement of my theorem.
\end{theorem}
\end{source}
To get a ``block'' of text, do this:
\begin{source}
\begin{block}{My block}
Content of my block.
\end{block}
\end{source}
\end{frame}

\begin{frame}[fragile]{Including graphics}
Graphics can be included as usual:
\begin{center}
\includegraphics[height=4cm]{clipboard.jpg}
\end{center}
\begin{source}
\begin{center}
\includegraphics[height=4cm]{clipboard.jpg}
\end{center}
\end{source}
\end{frame}

\begin{frame}[fragile]{Revealing a slide in stages}
\begin{block}{Euclid's algorithm} \pause 
To find \alert{$\gcd(a,b)$}, do the following: \pause
\begin{enumerate}
\item Change signs to get $a,b \geq 0$. \pause
\item Divide with remainder $a = q \cdot b + r$. \pause
\item Replace $(a,b)$ by $(b,r)$ and repeat. \pause
\item The last nonzero remainder is the gcd.
\end{enumerate}
\end{block}
\end{frame}

\begin{frame}[fragile]{Revealing a slide in stages}
The easiest way to achieve this effect is to use \verb|\pause|:
\begin{source}
\begin{block}{Euclid's algorithm} @\pause@
To find $\gcd(a,b)$, do the following: @\pause@
\begin{enumerate}
\item Change signs to get $a,b \geq 0$. @\pause@
\item Divide with remainder $a = q \cdot b + r$. @\pause@
\item Replace $(a,b)$ by $(b,r)$ and repeat. @\pause@
\item The last nonzero remainder is the gcd.
\end{enumerate}
\end{block}
\end{source}
\end{frame}

\begin{frame}[fragile]{Revealing a slide in stages}
There is a more compact notation for \verb|itemize| etc.
\begin{source}
\begin{enumerate}@[<+->]@
\item Change signs to get $a,b \geq 0$. 
\item Divide with remainder $a = q \cdot b + r$. 
\item Replace $(a,b)$ by $(b,r)$ and repeat. 
\item The last nonzero remainder is the gcd.
\end{enumerate}
\end{source}
You can also use the notation \verb|\item<1,3-4>| to tell Beamer on which slide(s) a
specific item should appear.
\end{frame}

\begin{frame}{Revealing a slide in stages}
\pause Only \pause use \pause this \pause technique \pause when \pause it \pause is \pause
\alert{really} \pause necessary!
\end{frame}

% TikZ
\part{Graphics with TikZ}
\begin{frame}
\partpage
\end{frame}

\begin{frame}[fragile]{What is TikZ?}
TikZ is a \LaTeX-package for creating graphics.
\begin{source}
\usepackage{@tikz@}

\begin{tikzpicture}
\end{tikzpicture}
\end{source}
There is no need to use an external graphics program.
\end{frame}


\begin{frame}{Example 1}
Here is a very basic example:
\begin{center}
\begin{tikzpicture}[scale=2]
\draw[color=violet,thick] (0,0) --(1,2) -- (3,3) -- (4,1) -- (2,0);
\draw[help lines] (0,0) grid (4,3);
\end{tikzpicture}
\end{center}
\end{frame}

\begin{frame}[fragile]{Example 1}
The basic drawing command is \verb|\draw|.
\begin{source}
\begin{tikzpicture}[scale=2]
@\draw@[color=violet,thick] (0,0) -- (1,2) -- (3,3) -- (4,1) -- (2,0);
@\draw@[help lines] (0,0) grid (4,3);
\end{tikzpicture}
\end{source}
Points are specified by their coordinates, such as \verb|(1,2)|.
\end{frame}


\begin{frame}{Example 2}
Here is an example from a calculus test:
\begin{center}
\begin{tikzpicture}[thick]
\draw [<->] (-5,0) -- (5,0);
\draw (0,0) -- (0,-2) -- (3,0);
\draw (1.5,0) node[anchor=south] {$x$};
\draw (0,-1) node[anchor=east] {$D$};
\draw (1.5,-1) node[anchor=west] {$\sqrt{D^2+x^2}$};
\draw (-5,0) node[anchor=south] {Highway};
\filldraw [black] (0,0) circle (1pt);
\filldraw [black] (0,-2) circle (1pt);
\draw (0,-2) node[anchor=north] {Observer};
\filldraw[thin,fill=gray!40] (2.9,-0.05) -- (2.9,0.05) -- (3.1,0.05) -- (3.1,-0.05) --
cycle;
\draw (3,0) node[anchor=south] {$\Delta x$};
\end{tikzpicture}
\end{center}
\end{frame}

\begin{frame}[fragile]{Example 2}
\begin{source}
\begin{tikzpicture}[thick]
\draw [<->] (-5,0) -- (5,0);
\draw (0,0) -- (0,-2) -- (3,0);
\draw (1.5,0) node[anchor=south] {$x$};
\draw (0,-1) node[anchor=east] {$D$};
\draw (1.5,-1) node[anchor=west] {$\sqrt{D^2+x^2}$};
\draw (-5,0) node[anchor=south] {Highway};
\filldraw [black] (0,0) circle (1pt);
\filldraw [black] (0,-2) circle (1pt);
@\draw (0,-2) node[anchor=north] {Observer};@
\filldraw[thin,fill=gray!40] (2.9,-0.05) -- (2.9,0.05) -- (3.1,0.05) -- (3.1,-0.05) --
cycle;
\draw (3,0) node[anchor=south] {$\Delta x$};
\end{tikzpicture}
\end{source}
\end{frame}

\begin{frame}[fragile]{Example 3}
Here is a more complicated example:
\begin{center}
\begin{tikzpicture}[scale=0.6]
\def\tower{ (0.6,4) -- (0.6,0.6) -- (1.2,0) -- (-1.2,0) -- (-0.6,0.6) -- (-0.6,4)} 
\draw[thick,fill=gray!40] \tower;
\pattern[pattern=bricks,pattern color=black] \tower;
\draw[very thick,fill=white] (0,6.55) circle (2.6);
\draw[<->] (1.4,0) -- node[fill=white] {$h$} (1.4,3.95);
\draw[<->] (3,0) -- node[fill=white] {$h+y$} (3,5.4);
\draw[thick,blue] (-2.32,5.4) -- (2.32,5.4);
\draw (0,6.55) -- node[sloped,anchor=south] {$r$} (2.32,5.4);
\draw[->,thick] (0,3.95) -- node[anchor=east] {$y$} (0,5.4)
	 -- node[anchor=east] {$r-y$} (0,6.55) -- (0,10) node[anchor=west] {$y$-axis};
\draw[<->] (1.4,0) -- node[fill=white] {$h$} (1.4,3.95);
\draw[<->,shorten >=2pt,shorten <=2pt]
	(0,6.55) -- node[sloped,fill=white] {$r$} (2.6,6.55);
\end{tikzpicture}
\end{center}
\end{frame}

\begin{frame}[fragile]{Example 3}
The base of the tower is drawn in the following way:
\begin{source}
\usetikzlibrary{patterns}

\begin{tikzpicture}
\def\tower{ (0.6,4) -- (0.6,0.6) -- (1.2,0) -- (-1.2,0) -- (-0.6,0.6) -- (-0.6,4)} 
\draw[thick,fill=gray!40] \tower;
\pattern[@pattern=bricks@,pattern color=black] \tower;
\end{tikzpicture}
\end{source}
\end{frame}

\begin{frame}[fragile]{Example 4}
Here is an example from graph theory:
\begin{center}
\begin{tikzpicture}[scale=0.35]
  \tikzstyle{vertex}=[circle,fill=black!10,minimum size=12pt,inner sep=1pt]
\node[vertex](A) at ( 5,10){A};
\node[vertex](D) at (15,10){D};
\node[vertex](E) at (20, 5){E};
\node[vertex](F) at (15, 0){F};
\node[vertex](C) at ( 5, 0){C};
\node[vertex](B) at ( 0, 5){B};
\path[draw,thick,-] (D) --  (A);
\path[draw,thick,-] (E) --  (D);
\path[draw,thick,-] (F) --  (E);
\path[draw,thick,-] (C) --  (F);
\path[draw,thick,-] (B) --  (C);
\path[draw,thick,-] (A) --  (B);
\path[draw,thick,-] (D) --  (C);
\path[draw,thick,-] (F) --  (D);
\path[draw,thick,-] (A) --  (C);
\end{tikzpicture}
\end{center}
\end{frame}

\begin{frame}[fragile]{Example 4}
Here is how to draw the vertices:
\begin{source}
\begin{tikzpicture}
\tikzstyle{vertex}=[circle,fill=black!10,minimum size=12pt,inner sep=1pt]
\node[vertex](A) at ( 5,10){A};
\node[vertex](D) at (15,10){D};
\path[draw,thick,-] (D) --  (A);
\end{tikzpicture}
\end{source}
\end{frame}

% Commutative diagrams
\part{Commutative diagrams with tikz-cd}
\begin{frame}
\partpage
\end{frame}

\begin{frame}[fragile]{What is tikz-cd?}
tikz-cd is a \LaTeX-package for commutative diagrams. 

Commutative diagrams are pictures like this one:
\[
\begin{tikzcd}
	A \dar{g} \rar{f} & B \dar{h} \\
	C \rar{i} & D
\end{tikzcd}
\]
They are used a lot in algebraic geometry, category theory, etc.  
\end{frame}

\begin{frame}[fragile]{How to create a basic diagram?}
Here is the same diagram without the arrows:
\begin{equation*}
\begin{tikzcd}
	A & B \\
	C & D
\end{tikzcd}
\end{equation*}
The objects form an array, separated with \verb|&| and \verb|\\|.
\begin{source}
\usepackage{tikz,@tikz-cd@}

\begin{equation*}
\begin{tikzcd}
	A & B \\
	C & D
\end{tikzcd}
\end{equation*}
\end{source}
\end{frame}

\begin{frame}[fragile]{How to create a basic diagram?}
Here is the diagram again, this time with arrows:
\[
\begin{tikzcd}
	A \dar{g} \rar{f} & B \dar{h} \\
	C \rar{i} & D
\end{tikzcd}
\]
The arrows, in shorthand notation, are \verb|\dar|, \verb|\uar|, etc. 
\begin{source}
\begin{tikzcd}
	A \dar{g} \rar{f} & B \dar{h} \\
	C \rar{i} & D
\end{tikzcd}
\end{source}
Arrow commands come \alert{after} the object where the arrow starts.
\end{frame}


\begin{frame}[fragile]{More complicated arrows}
\[
\begin{tikzcd}
A \rar[dashed]{f} \arrow[bend right=20]{drr}{j} \arrow[bend left=40]{rr}{i} 
		& B \rar[hook,color=blue]{g} & C \dar[swap]{h} \\
 & & D
\end{tikzcd}
\]
\begin{source}
\begin{tikzcd}
A \rar[dashed]{f} \arrow[bend right=20]{drr}{j} \arrow[bend left=40]{rr}{i} & 
B \rar[hook,color=blue]{g} & C \dar[swap]{h} \\
& & D
\end{tikzcd}
\end{source}
\end{frame}

\begin{frame}[fragile]{Arrows that cross each other}
Here is a 3-dimensional example:
\[
\begin{tikzcd}[row sep=scriptsize, column sep=scriptsize]
  & f^* E_V \arrow{dl}\arrow{rr}\arrow{dd} & & E_V \arrow{dl}\arrow{dd} \\
  f^* E \arrow[crossing over]{rr}\arrow{dd} & & E \\
  & U \arrow{dl}\arrow{rr} &  & V \arrow{dl} \\
  M \arrow{rr} & & N\arrow[crossing over, leftarrow]{uu}\\
\end{tikzcd}
\vspace{-15pt}
\]
To get this effect:
\begin{itemize}
\item Adjust the spacing between rows and columns.
\item Tell tikz-cd which arrows cross over each other.
\item Reverse the direction of certain arrows.
\end{itemize}
\end{frame}

\begin{frame}[fragile]{Arrows that cross each other}
Here is the tikz-cd code for the example:
\begin{source}
\begin{tikzcd}[row sep=scriptsize, column sep=scriptsize]
  & f^* E_V \arrow{dl}\arrow{rr}\arrow{dd} & & E_V \arrow{dl}\arrow{dd} \\
  f^* E \arrow[crossing over]{rr}\arrow{dd} & & E \\
  & U \arrow{dl}\arrow{rr} &  & V \arrow{dl} \\
  M \arrow{rr} & & N @\arrow[crossing over, leftarrow]{uu}@ \\
\end{tikzcd}
\end{source}
\end{frame}


\begin{frame}
\begin{center}
{\LARGE Thank you!}
\end{center}
\end{frame}

\end{document}
